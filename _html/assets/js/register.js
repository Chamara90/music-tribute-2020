/**
 *
 * @mainSection: Register;
 * @dateCreated: 20191205;
 * @by : lakmal@fortunaglobal.com;
 *
 */


var preloaderHide = function(){
    $('.preloader').fadeOut(300);
};

var preloaderShow = function(){
    $('.preloader').fadeIn(300);
};


(function( $ ) {
    "use strict";

    $('#register-form').submit(function(e){
        e.preventDefault();
    });

    $('#register-form').validate({
        ignore: [],
        rules: {
            name: {
                required: true,
                normalizer: function( value ) {
                    return $.trim( value );
                },
                maxlength: 50,
            },
            username: {
                required: true,
                normalizer: function( value ) {
                    return $.trim( value );
                },
                customEmail: true,
                maxlength: 100,
            },
            password: {
                required: true,
                normalizer: function( value ) {
                    return $.trim( value );
                },
                minlength: 7,
                maxlength: 50,
            },
            confirm_password: {
                equalTo: "#password"
            },
            tnc: {
                required: true,
            }
        },
        messages: {
            confirm_password:{
                equalTo: "Password fields do not match!",
            }
        },
        submitHandler: function(form) {
            form.submit();
            // $.ajax({
            //     url: '',
            //     type: 'POST',
            //     dataType: 'JSON',
            //     headers: {
            //         //'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            //     },
            //     data: $(form).serialize(),
            //     beforeSend: function () {
            //         preloaderShow();
            //     },
            //     complete: function () {
            //         preloaderHide();
            //     },
            //     success: function(response){
            //         if(response.success){

            //         } else{

            //         }
            //     }
            // });
        }
    });


}(jQuery));


