/**
 *
 * @mainSection: Listing;
 * @dateCreated: 20191212;
 * @by : lakmal@fortunaglobal.com;
 *
 */



(function($) {
    "use strict";

    $(function() {
        $('[data-toggle="social-share-tooltip"]').tooltip();
    });

    $('.phone-number-toggle').find('a').on('click', function(e){
        e.preventDefault();
        if ($(this).hasClass('clicked')) {
            var call = $(this).attr('href');
            location.href = call;
        } else{
            var $dataPost = $(this).attr('data-post');
            $(this).find('.phone-number-outer').html($dataPost);
            $(this).addClass('clicked');
        }
    });

})(jQuery);

(function($) {
    "use strict";

    var bigimage = $('.gallery-hero');
    var thumbs = $(".gallery-thumbs");
    var syncedSecondary = true;

    bigimage.owlCarousel({
        items: 1,
        smartSpeed: 600,
        slideSpeed: 600,
        nav: true,
        autoplay: false,
        dots: false,
        loop: true,
        responsiveRefreshRate: 200
    }).on("changed.owl.carousel", syncPosition);

    thumbs.on("initialized.owl.carousel", function() {
        thumbs.find(".owl-item").eq(0).addClass("current");
    }).owlCarousel({
        items: 4,
        dots: false,
        nav: true,
        smartSpeed: 600,
        slideSpeed: 600,
        slideBy: 4,
        margin: 10,
        responsiveRefreshRate: 100,
        responsive:{
            576: {
                margin: 15,
            },
        }
    }).on("changed.owl.carousel", syncPosition2);

    function syncPosition(el) {
        var count = el.item.count - 1;
        var current = Math.round(el.item.index - el.item.count / 2 - 0.5);

        if (current < 0) {
            current = count;
        }
        if (current > count) {
            current = 0;
        }

        thumbs.find(".owl-item").removeClass("current").eq(current).addClass("current");
        var onscreen = thumbs.find(".owl-item.active").length - 1;
        var start = thumbs.find(".owl-item.active").first().index();
        var end = thumbs.find(".owl-item.active").last().index();

        if (current > end) {
            thumbs.data("owl.carousel").to(current, 600, true);
        }
        if (current < start) {
            thumbs.data("owl.carousel").to(current - onscreen, 600, true);
        }
    }

    function syncPosition2(el) {
        if (syncedSecondary) {
            var number = el.item.index;
            bigimage.data("owl.carousel").to(number, 600, true);
        }
    }

    thumbs.on("click", ".owl-item", function(e) {
        e.preventDefault();
        var number = $(this).index();
        bigimage.data("owl.carousel").to(number, 600, true);
    });

}(jQuery));


