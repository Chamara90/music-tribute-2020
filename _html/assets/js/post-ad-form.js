/**
 *
 * @mainSection: Edit Profile;
 * @dateCreated: 20191205;
 * @by : lakmal@fortunaglobal.com;
 *
 */


// Post ad form validation
(function( $ ) {
    "use strict";
    $('#adz-form').submit(function(e){
        console.log("in the form")
        e.preventDefault();
    });

    $('#adz-form').validate({
        ignore: [],
        rules: {
            fname: {
                required: true,
                normalizer: function( value ) {
                    return $.trim( value );
                },
                maxlength: 50,
            },
            lname: {
                required: true,
                normalizer: function( value ) {
                    return $.trim( value );
                },
                maxlength: 50,
            },
            province: {
                required: true,
                normalizer: function( value ) {
                    return $.trim( value );
                },
                maxlength: 50,
            },
            city: {
                required: true,
                normalizer: function( value ) {
                    return $.trim( value );
                },
                maxlength: 50,
            },
            username: {
                required: true,
                normalizer: function( value ) {
                    return $.trim( value );
                },
                customEmail: true,
                maxlength: 100,
            },
        },
        submitHandler: function(form) {
            $.ajax({
                url: '',
                type: 'POST',
                dataType: 'JSON',
                headers: {
                    //'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                data: $(form).serialize(),
                beforeSend: function () {
                    preloaderShow();
                },
                complete: function () {
                    preloaderHide();
                },
                success: function(response){
                    if(response.success){

                    } else{

                    }
                }
            });
        }
    });

}(jQuery));






