/**
 *
 * @mainSection: Login;
 * @dateCreated: 20191205;
 * @by : lakmal@fortunaglobal.com;
 *
 */


var preloaderHide = function(){
    $('.preloader').fadeOut(300);
};

var preloaderShow = function(){
    $('.preloader').fadeIn(300);
};


(function( $ ) {

    $('#login-form').submit(function(e){
        e.preventDefault();
    });

    $('#login-form').validate({
        ignore: [],
        rules: {
            username: {
                required: true,
                normalizer: function( value ) {
                    return $.trim( value );
                },
            },
            password: {
                required: true,
                normalizer: function( value ) {
                    return $.trim( value );
                },
            },
        },
        submitHandler: function(form) {
            form.submit();
            // $.ajax({
            //     url: '',
            //     type: 'POST',
            //     dataType: 'JSON',
            //     headers: {
            //         'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            //     },
            //     data: $(form).serialize(),
            //     beforeSend: function () {
            //         preloaderShow();
            //     },
            //     complete: function () {
            //         preloaderHide();
            //     },
            //     success: function(response){
            //         if(response.success){

            //         } else{

            //         }
            //     }
            // });
        }
    });


}(jQuery));


