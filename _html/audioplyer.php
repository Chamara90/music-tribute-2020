    <link href="MINImusic-Player-master/css/styles.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript" src="MINImusic-Player-master/js/musicplayer.js"></script>
    <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.9.0/slick.css" />
    <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.9.0/slick-theme.min.css" />

    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.9.0/slick.min.js"></script>

    <script type="text/javascript">
        $(".example").musicPlayer({
            elements: ['artwork', 'information', 'controls', 'progress','shere'], // ==> This will display in  the order it is inserted
            //elements: [ 'controls' , 'information', 'artwork', 'progress', 'time', 'volume' ],
            controlElements: ['play'],
            //timeElements: ['current', 'duration'],
            //timeSeparator: " : ", // ==> Only used if two elements in timeElements option
            //infoElements: [  'title', 'artist' ],

            //volume: 10,
            //autoPlay: true,
            //loop: true,

            // onPlay: function () {
            //     $('body').css('background', 'black');
            // },
            // onPause: function () {
            //     $('body').css('background', 'green');
            // },
            // onStop: function () {
            //     $('body').css('background', '#141942');
            // },
            // onFwd: function () {
            //     $('body').css('background', 'white');
            // },
            // onRew: function () {
            //     $('body').css('background', 'blue');
            // },
            // volumeChanged: function () {
            //     $('body').css('background', 'red');
            // },
            // progressChanged: function () {
            //     $('body').css('background', 'orange');
            // },
            // trackClicked: function () {
            //     $('body').css('background', 'brown');
            // },
            // onMute: function () {
            //     $('body').css('background', 'grey');
            // },
        });


$(".music-image img").each(function(index) {
    var src = $(this).attr("src");
    var eventCal = $(this).parent();

    eventCal.css("background", "url(" + src + ") center center/cover no-repeat");
    $(this).hide();
});
    </script>
