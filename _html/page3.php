<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
    <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' name='viewport' />
    <title>Music</title>
    <?php include 'includes/common-doc-head.php'; ?>
    <style>
        .interview-video {
            margin-bottom: 1rem;
        }
        .interview-video p {
            height: 38px;
        }
        .nav-tabs {
            border-bottom: none;
            width: 100px;
            margin: 0 auto;
            margin-top: 1rem;
        }

        .nav-tabs .nav-link {
            color: #000;
            background-color: #fff;
            border-radius: 0;
            border-color: #ccc;
        }
        .nav-link .active {
            color: red;
            background-color: blue;
            border-color: #000;
        }
        
        .nav-tabs .nav-link:hover {
            border-color: #ccc;
            background-color: #ccc;
            border-color: #ccc;
        }

        .nav-tabs .nav-link.active, .nav-tabs .nav-item.show .nav-link {
            color: #fff;
            background-color: #000;
            border-color: #dee2e6 #dee2e6 #fff;
        }
        
        .song-item {
            margin-bottom: 2rem;
        }
        
        .song-item p {
            margin-top: 1rem;
            text-align: center;
        }
        
        .song-item p a {
            color: #828282;
            border: 1px solid #d6d6d6;
            padding: 8px 20px;
            -webkit-transition: 0.2s ease;
            -moz-transition: 0.2s ease;
            -o-transition: 0.2s ease;
            -ms-transition: 0.2s ease;
            transition: 0.2s ease;
        }
        
        .song-item p a:hover {
            color: #000;
            background-color: #d6d6d6; 
        }
    </style>
</head>
<body>
    <?php include 'includes/page-header.php'; ?>

    <main id="landing-page" class="main-content pt-0">
        <div class="container position">
            <div class="list-item-gallery ">
                  <div class="top-main-banner w-75">
                      <img src="assets/images/filte1.jpg" alt="Music">
                  </div>
            </div>
            <div class="main-banner">
                <div class="gray-blure"></div>
            </div>
        </div>
        <div class="container">
            <nav class="navbar navbar-expand-lg navbar-light w-75 mx-auto p-0  mt-2">

              <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
              </button>

              <div class="collapse navbar-collapse row d-flex justify-content-end no-gutters" id="navbarSupportedContent">
                <ul class="navbar-nav col-12 col-sm-5">
                  <li class="nav-item active">
                    <a class="nav-link" href="#">All <span class="sr-only">(current)</span></a>
                  </li>
                  <li class="nav-item">
                    <a class="nav-link" href="#">Official</a>
                  </li>
                  <li class="nav-item">
                    <a class="nav-link" href="#">Interviews</a>
                  </li>
                  <li class="nav-item">
                    <a class="nav-link" href="#">Short Films</a>
                  </li>
                </ul>
                <form class="col-12 col-sm-5">
                    <div class="">
                        <div class="input-group">
                            
                            <input type="text" class="form-control" name="searchtext" placeholder="SEARCH MOVIES & TV">
                            <div class="input-group-append">
                                <button type="submit" class="btn btn-search">
                                    <i aria-hidden="true"><svg version="1.1" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="16px" height="16px" viewBox="0 0 16 16">
                                    <path d="M15.561,13.438l-3.672-3.67c-0.546,0.85-1.27,1.574-2.119,2.121l3.67,3.671c0.586,0.586,1.536,0.586,2.121,0 C16.146,14.975,16.146,14.025,15.561,13.438"></path>
                                    <path d="M11.999,6c0-3.313-2.686-6-5.999-6C2.686,0,0,2.687,0,6s2.686,5.999,6,5.999 C9.313,11.999,11.999,9.313,11.999,6 M6,10.499c-2.481,0-4.5-2.018-4.5-4.499S3.519,1.5,6,1.5s4.5,2.019,4.5,4.5 S8.481,10.499,6,10.499"></path></svg></i>
                                </button>
                            </div>
                        </div>
                    </div>
                </form>
              </div>
            </nav>
        </div>
        <div class="main-content-inner">
            <div class="preloader"><span></span></div>
            <div class="page-content">
                <br>
                <br> 
                <div class="container">
                    <div class="tab-content">
                        
                        <div class="tab-pane active" id="tabs-1" role="tabpanel">
                            <div class="container">
                                <div class="row">
                                    <div class="col-lg-3 col-md-4 col-sm-12">
                                        <div class="song-item">
                                            <h3>Karuna Meth Mudhitha</h3>
                                            <a href="karuna-meth-mudhitha.php"><img alt="Song Cover Image" class="img-fluid" src="assets/images/cover-image/sith-ahasa-cover-img.jpg"></a>
                                            <p><a href="karuna-meth-mudhitha.php">Read More</a></p>
                                        </div>                                        
                                    </div>
                                    
                                    <div class="col-lg-3 col-md-4 col-sm-12">
                                        <div class="song-item">
                                            <h3>Aparimitha Wu</h3>
                                            <a href="aparimitha-wu.php"><img alt="Song Cover Image" class="img-fluid" src="assets/images/cover-image/sith-ahasa-cover-img.jpg"></a>
                                            <p><a href="aparimitha-wu.php">Read More</a></p>
                                        </div>                                        
                                    </div>
                                    
                                    <div class="col-lg-3 col-md-4 col-sm-12">
                                        <div class="song-item">
                                            <h3>Piyanane</h3>
                                            <a href="piyanane.php"><img alt="Song Cover Image" class="img-fluid" src="assets/images/cover-image/sith-ahasa-cover-img.jpg"></a>
                                            <p><a href="piyanane.php">Read More</a></p>
                                        </div>                                        
                                    </div>
                                    
                                    <div class="col-lg-3 col-md-4 col-sm-12">
                                        <div class="song-item">
                                            <h3>Dolos Mahe Sanda</h3>
                                            <a href="dolos-mahe-sanda.php"><img alt="Song Cover Image" class="img-fluid" src="assets/images/cover-image/sith-ahasa-cover-img.jpg"></a>
                                            <p><a href="dolos-mahe-sanda.php">Read More</a></p>
                                        </div>                                        
                                    </div>
                                    
                                    <div class="col-lg-3 col-md-4 col-sm-12">
                                        <div class="song-item">
                                            <h3>Manamaliya Wee</h3>
                                            <a href="manaliya-wee.php"><img alt="Song Cover Image" class="img-fluid" src="assets/images/cover-image/sith-ahasa-cover-img.jpg"></a>
                                            <p><a href="manaliya-wee.php">Read More</a></p>
                                        </div>                                        
                                    </div>
                                    
                                    <div class="col-lg-3 col-md-4 col-sm-12">
                                        <div class="song-item">
                                            <h3>Sewanellak Wee</h3>
                                            <a href="sevanellak-wee.php"><img alt="Song Cover Image" class="img-fluid" src="assets/images/cover-image/sith-ahasa-cover-img.jpg"></a>
                                            <p><a href="sevanellak-wee.php">Read More</a></p>
                                        </div>                                        
                                    </div>
                                    
                                    <div class="col-lg-3 col-md-4 col-sm-12">
                                        <div class="song-item">
                                            <h3>Sande Sisilath</h3>
                                            <a href="sade-sisilath.php"><img alt="Song Cover Image" class="img-fluid" src="assets/images/cover-image/sith-ahasa-cover-img.jpg"></a>
                                            <p><a href="sade-sisilath.php">Read More</a></p>
                                        </div>                                        
                                    </div>
                                    
                                    <div class="col-lg-3 col-md-4 col-sm-12">
                                        <div class="song-item">
                                            <h3>Pipi Mal Gomuwe</h3>
                                            <a href="pipi-mal-gomuwe.php"><img alt="Song Cover Image" class="img-fluid" src="assets/images/cover-image/sith-ahasa-cover-img.jpg"></a>
                                            <p><a href="pipi-mal-gomuwe.php">Read More</a></p>
                                        </div>                                        
                                    </div>
                                    
                                    <div class="col-lg-3 col-md-4 col-sm-12">
                                        <div class="song-item">
                                            <h3>Nuba Lagin</h3>
                                            <a href="nuba-lagin.php"><img alt="Song Cover Image" class="img-fluid" src="assets/images/cover-image/sith-ahasa-cover-img.jpg"></a>
                                            <p><a href="nuba-lagin.php">Read More</a></p>
                                        </div>                                        
                                    </div>
                                    
                                    <div class="col-lg-3 col-md-4 col-sm-12">
                                        <div class="song-item">
                                            <h3>Nuba Awidin</h3>
                                            <a href="nuba-awidin.php"><img alt="Song Cover Image" class="img-fluid" src="assets/images/cover-image/sith-ahasa-cover-img.jpg"></a>
                                            <p><a href="nuba-awidin.php">Read More</a></p>
                                        </div>                                        
                                    </div>
                                    
                                    <div class="col-lg-3 col-md-4 col-sm-12">
                                        <div class="song-item">
                                            <h3>Sith Ahasa</h3>
                                            <a href="sith-ahasa.php"><img alt="Song Cover Image" class="img-fluid" src="assets/images/cover-image/sith-ahasa-cover-img.jpg"></a>
                                            <p><a href="sith-ahasa.php">Read More</a></p>
                                        </div>                                        
                                    </div>
                                    
                                    <div class="col-lg-3 col-md-4 col-sm-12">
                                        <div class="song-item">
                                            <h3>Yanna Denna Mata</h3>
                                            <a href="yanna-denna-mata.php"><img alt="Song Cover Image" class="img-fluid" src="assets/images/cover-image/sith-ahasa-cover-img.jpg"></a>
                                            <p><a href="yanna-denna-mata.php">Read More</a></p>
                                        </div>                                        
                                    </div>

                                    



                                
                                </div>
                            </div>
                        </div>
                        
                        <div class="tab-pane" id="tabs-2" role="tabpanel">
                            <div class="container">
                                <div class="row">
                                    
                                    <div class="col-lg-3 col-md-4 col-sm-12">
                                        <div class="song-item">
                                            <h3>Pinna Wetena</h3>
                                            <a href="pinna-wetena.php"><img alt="Song Cover Image" class="img-fluid" src="assets/images/cover-image/sith-ahasa-cover-img.jpg"></a>
                                            <p><a href="pinna-wetena.php">Read More</a></p>
                                        </div>                                        
                                    </div>
                                    
                                    <div class="col-lg-3 col-md-4 col-sm-12">
                                        <div class="song-item">
                                            <h3>Mal Hasarel</h3>
                                            <a href="mal-hasarel.php"><img alt="Song Cover Image" class="img-fluid" src="assets/images/cover-image/sith-ahasa-cover-img.jpg"></a>
                                            <p><a href="mal-hasarel.php">Read More</a></p>
                                        </div>                                        
                                    </div>
                                    
                                    <div class="col-lg-3 col-md-4 col-sm-12">
                                        <div class="song-item">
                                            <h3>Oba Pewa Sina</h3>
                                            <a href="oba-peewa-sina.php"><img alt="Song Cover Image" class="img-fluid" src="assets/images/cover-image/sith-ahasa-cover-img.jpg"></a>
                                            <p><a href="oba-peewa-sina.php">Read More</a></p>
                                        </div>                                        
                                    </div>
                                    
                                    <div class="col-lg-3 col-md-4 col-sm-12">
                                        <div class="song-item">
                                            <h3>Dethol Wila Matha</h3>
                                            <a href="dethol-wila-matha.php"><img alt="Song Cover Image" class="img-fluid" src="assets/images/cover-image/sith-ahasa-cover-img.jpg"></a>
                                            <p><a href="dethol-wila-matha.php">Read More</a></p>
                                        </div>                                        
                                    </div>
                                    
                                    <div class="col-lg-3 col-md-4 col-sm-12">
                                        <div class="song-item">
                                            <h3>Sithe Siraunu</h3>
                                            <a href="sithe-siraunu.php"><img alt="Song Cover Image" class="img-fluid" src="assets/images/cover-image/sith-ahasa-cover-img.jpg"></a>
                                            <p><a href="sithe-siraunu.php">Read More</a></p>
                                        </div>                                        
                                    </div>
                                    
                                    <div class="col-lg-3 col-md-4 col-sm-12">
                                        <div class="song-item">
                                            <h3>Nuba Kohedai</h3>
                                            <a href="nuba-kohedai.php"><img alt="Song Cover Image" class="img-fluid" src="assets/images/cover-image/sith-ahasa-cover-img.jpg"></a>
                                            <p><a href="nuba-kohedai.php">Read More</a></p>
                                        </div>                                        
                                    </div>
                                    
                                    <div class="col-lg-3 col-md-4 col-sm-12">
                                        <div class="song-item">
                                            <h3>Lagin Hitiyath</h3>
                                            <a href="lagin-hitiyath.php"><img alt="Song Cover Image" class="img-fluid" src="assets/images/cover-image/sith-ahasa-cover-img.jpg"></a>
                                            <p><a href="lagin-hitiyath.php">Read More</a></p>
                                        </div>                                        
                                    </div>
                                    
<!--
                                    <div class="col-lg-3 col-md-4 col-sm-12">
                                        <div class="song-item">
                                            <h3>SithAhasa</h3>
                                            <a href="aaa"><img alt="Song Cover Image" class="img-fluid" src="assets/images/cover-image/sith-ahasa-cover-img.jpg"></a>
                                            <p><a href="aaa">Read More</a></p>
                                        </div>                                        
                                    </div>
-->
                                    
<!--
                                    <div class="col-lg-3 col-md-4 col-sm-12">
                                        <div class="song-item">
                                            <h3>SithAhasa</h3>
                                            <a href="aaa"><img alt="Song Cover Image" class="img-fluid" src="assets/images/cover-image/sith-ahasa-cover-img.jpg"></a>
                                            <p><a href="aaa">Read More</a></p>
                                        </div>                                        
                                    </div>
-->
                                    
<!--
                                    <div class="col-lg-3 col-md-4 col-sm-12">
                                        <div class="song-item">
                                            <h3>SithAhasa</h3>
                                            <a href="aaa"><img alt="Song Cover Image" class="img-fluid" src="assets/images/cover-image/sith-ahasa-cover-img.jpg"></a>
                                            <p><a href="aaa">Read More</a></p>
                                        </div>                                        
                                    </div>
-->
                                    
                                    

                                </div>
                            </div>
                        </div>
                        
                    </div>
                    <ul class="nav nav-tabs" role="tablist">
                        <li class="nav-item">
                            <a class="nav-link active" data-toggle="tab" href="#tabs-1" role="tab">1</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" data-toggle="tab" href="#tabs-2" role="tab">2</a>
                        </li>
                    </ul><!-- Tab panes -->
                    
                    
                
                </div>
   
    </main>
    <?php include 'includes/page-footer.php'; ?>
    <?php include 'includes/common-scripts.php'; ?>
 
     <?php include 'audioplyer.php'; ?>
</body>
</html>
    <style>
        #show-more-container {
            display: none;
            width: 100%;
        }
    </style>
    
<!--
    <script>
        
        $( document ).ready(function() {
            $("#show-more").click(function(){
              $("#show-more-container").slideDown();
            });
        });
    
    </script>
-->
