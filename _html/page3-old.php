<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
    <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' name='viewport' />
    <title>Music</title>
    <?php include 'includes/common-doc-head.php'; ?>
</head>
<body>
    <?php include 'includes/page-header.php'; ?>

    <main id="landing-page" class="main-content pt-0">
        <div class="container">
            <div class="border-bottom pt-3 pb-3">
                <h1 class="mt-3">His Eye Is On The Sparrow</h1>
                <div class="row d-flex align-items-center">
                    <div class="col-sm-5">
                        <img class="img-fluid" src="assets/images/cover_img.jpg">
                    </div>
                    <div class="col-sm-4">
                        <div class="music-image mb-1">
                            <div class="player-details">
                                <p>Songwriter : <span>C.D Martin, Charles Gabriel</span></p>
                                <p>Artist : <span>C.D Martin, Charles Gabriel</span></p>
                                <p>Music : <span>C.D Martin, Charles Gabriel</span></p>
                            </div>
                            <div class="player-container">
                                <iframe src="https://open.spotify.com/embed/album/16rTsMjlDt6DEbLRtxvcWu" width="" height="" frameborder="0" allowtransparency="true" allow="encrypted-media"></iframe>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="container">
            <div class="border-bottom pt-4 pb-4">
                <h1>LYRICS</h1>
                <div class="row content">
                    <div class="col-5">
                        <p class="mb-0">Why should I feel discouraged,</p>
                        <p class="mb-0">Why should the shadows come,</p>
                        <p class="mb-0">Why should my heart be lonely,</p>
                        <p class="mb-0">And long for Heaven, Heaven and home,</p>
                        <p class="mb-0">When, When jesus in my portion,</p>
                        <p class="mb-0">My constant friend in He;</p>
                        <p class="mb-0">Oh, oh-oh, His eye is on the sperrow,</p>
                        <p class="mb-0">And I know He watches, watches over me</p>
                    </div>
                    <div class="col-5">
                        <p class="mb-0">Why should I feel discouraged,</p>
                        <p class="mb-0">Why should the shadows come,</p>
                        <p class="mb-0">Why should my heart be lonely,</p>
                        <p class="mb-0">And long for Heaven, Heaven and home,</p>
                    </div>
                </div>
            </div>
        </div>
        <div class="container">
            <div class="border-bottom pt-3 pb-3">
                <h1>VIDEOS</h1>
                <div class="row">
<!--
                    <div class="col-md-3">
                        <div class="backdrop-content level-lats col-sm-6">
                            <iframe src="https://www.youtube.com/embed/H1Yt0xJKDY8" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                        </div> 
                    </div>
-->
                    <div class="col-md-4">
                            <iframe style="width:100%;height:220px;margin-bottom:1rem;" src="https://www.youtube.com/embed/-YA8Uh8u0vo" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                    </div>
                    
                    <div class="col-md-4">
                            <iframe style="width:100%;height:220px;margin-bottom:1rem;" src="https://www.youtube.com/embed/X5lRvg3AHzU" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                    </div>
                    
                    <div class="col-md-4">
                            <iframe style="width:100%;height:220px;" src="https://www.youtube.com/embed/g0Sorn1M2ns" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                    </div>
                    
                    <div class="col-md-4">
                            <iframe style="width:100%;height:220px;" src="https://www.youtube.com/embed/gFMZdaOlzKg" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                    </div>

                    
                </div>
            </div>
        </div>
        <div class="container">
            <div class="border-bottom pt-3 pb-3">
                <div class="row">
                    <div class="col-12">
                        <a class="btn hide-comment p-0" data-toggle="collapse" href="#collapseExample" role="button" aria-expanded="false" aria-controls="collapseExample">
                            HIDE COMMENTS
                        </a>

                    <div class="collapse show" id="collapseExample">
                        <div class="row">
                            <div class="fb-feeds col-sm-6">
                                <div id="fb-root"></div>
                                    <script async defer crossorigin="anonymous" src="https://connect.facebook.net/en_US/sdk.js#xfbml=1&version=v6.0"></script>
                                <div class="fb-comments" data-href="https://www.facebook.com/richardmarxmusic/" data-width="60%" data-numposts="5"></div>
                            </div>
                        </div>
                    </div> 
                    </div>
                </div>
            </div>
        </div>


    </main>
    <?php include 'includes/page-footer.php'; ?>
    <?php include 'includes/common-scripts.php'; ?>
 
<script type="text/javascript">
    $('.hide-comment').click(function(){
        var $this = $(this);
        $this.toggleClass('hide-comment');
        if($this.hasClass('hide-comment')){
            $this.text('HIDE COMMENTS');         
        } else {
            
            $this.text('SHOW COMMENTS');
        }
    });
</script>
</body>
</html>
