<footer id="footer-1">
    <div class="container">
        <div class="footer-top">
            <div class="row">
                <div class="footer-nav" >
                    <ul>
                        <li><a><span><i class="fa fa-instagram" aria-hidden="true"></i></span></a></li>
                        <li><a><span><i class="fa fa-facebook" aria-hidden="true"></i></span></a></li>
                        <li><a><span><i class="fa fa-twitter" aria-hidden="true"></i></span></a></li>
                        <li><a>Store</a></li>
                        <li><a>Cookies</a></li>
                        <li><a>Privacy Policy</a></li>
                        <li><a>Contact</a></li>
                    </ul>
                <div>
                <!-- <div class="col-sm-6 col-md-3">
                    <nav class="footer-links text-center text-sm-left">
                        <h3 class="text-uppercase">Search More</h3>
                        <ul class="nav flex-column">
                            <li class="nav-item">
                                <a class="nav-link" href="#">Promote your ad</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="#">Banner advertising</a>
                            </li>
                        </ul>
                    </nav>
                </div> -->
                <!-- <div class="col-sm-6 col-md-3">
                    <nav class="footer-links text-center text-sm-left">
                        <h3 class="text-uppercase">Help &amp; Support</h3>
                        <ul class="nav flex-column">
                            <li class="nav-item">
                                <a class="nav-link" href="faq.php">FAQ</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="stay-safe.php">Stay safe on topads.lk</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="contact-us.php">Contact Us</a>
                            </li>
                        </ul>
                    </nav>
                </div> -->
                <!-- <div class="col-sm-6 col-md-3">
                    <nav class="footer-links text-center text-sm-left">
                        <h3 class="text-uppercase">About Us</h3>
                        <ul class="nav flex-column">
                            <li class="nav-item">
                                <a class="nav-link" href="#">What we do</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="#">Career</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="terms-conditions.php">Terms &amp; Conditions</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="privacy-policy.php">Privacy Policy</a>
                            </li>
                        </ul>
                    </nav>
                </div> -->
                <!-- <div class="col-sm-6 col-md-3">
                    <figure class="footer-logo text-center text-sm-left text-md-right">
                        <a href="http://topads.lk/">
                            <img src="assets/images/topads-logo-footer.png" class="img-fluid" alt="TopAds">
                        </a>
                    </figure>
                </div> -->
            </div>
        </div>
    </div>
</footer>
