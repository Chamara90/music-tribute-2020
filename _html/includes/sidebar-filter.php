<div class="filter-container">
    <div class="filter-section sort-by-dropdown">
        <button class="btn btn-link fs-toggle" type="button">
            Sort results by
        </button>
        <div class="form-group">
            <select id="filter-sort-by" class="custom-select">
                <option value="">Date: Newest on top</option>
                <option value="">Date: Oldest on top</option>
                <option value="">Price: High to low</option>
                <option value="">Price: Low to high</option>
            </select>
        </div>
    </div>
    <div class="filter-section filter-category">
        <button class="btn btn-link fs-toggle" type="button" data-toggle="collapse" aria-expanded="true">
            Category <i class="fa fa-angle-down" aria-hidden="true"></i>
        </button>
        <div class="collapse show fs-collapse">
            <div class="collapse-inner">
                <ul class="fs-header">
                    <li>
                        <a href="#" class="btn btn-link">
                            <span>All Categories</span>
                        </a>
                    </li>
                </ul>
                <ul class="fs-links level1">
                    <li>
                        <a href="#" class="btn btn-link">
                            <span class="icon-filter">
                                <svg width="24" height="24" viewBox="0 0 24 24" class="svg-wrapper--8ky9e"><defs><path id="vehicles_svg__a" d="M.032.03H21.86v17.244H.032z"></path><path id="vehicles_svg__c" d="M.146.03H11.06v17.244H.146z"></path></defs><g fill="none" fill-rule="evenodd"><path d="M0 8.571c0 .73.592 1.323 1.323 1.323h1.984a1.323 1.323 0 0 0 0-2.646H1.323C.593 7.248 0 7.841 0 8.571M19.182 8.571c0 .73.592 1.323 1.323 1.323h1.984a1.323 1.323 0 0 0 0-2.646h-1.984c-.73 0-1.323.593-1.323 1.323" fill="#00997A"></path><g transform="translate(.96 2.21)"><mask id="vehicles_svg__b" fill="#fff"><use xlink:href="#vehicles_svg__a"></use></mask><path d="M21.847 11.52c-.041-.391-.354-2.745-.8-3.433l-.84-2.766S19.025 1.044 16.65.545C15.02.203 13.59.022 10.946.03 8.302.022 6.872.203 5.24.545c-2.375.499-3.555 4.776-3.555 4.776l-.84 2.766c-.447.688-.76 3.042-.801 3.433H.039v5.38l-.007.374H21.86l-.008-.374v-5.38h-.005z" fill="#009F7F" mask="url(#vehicles_svg__b)"></path></g><g transform="translate(11.76 2.21)"><mask id="vehicles_svg__d" fill="#fff"><use xlink:href="#vehicles_svg__c"></use></mask><path d="M10.246 8.087l-.84-2.766S8.226 1.044 5.851.545C4.22.203 2.79.022.146.03v17.244H11.06l-.008-.374v-5.38h-.005c-.041-.391-.354-2.745-.8-3.433" fill="#008D70" mask="url(#vehicles_svg__d)"></path></g><path d="M3.969 15.516a1.323 1.323 0 1 1 0-2.647 1.323 1.323 0 0 1 0 2.647" fill="#E4E3D7"></path><path d="M4.673 9.894c-.437 0-.402-.455-.402-.455S5.443 5.905 5.518 5.7a.658.658 0 0 1 .55-.436h11.61c.052 0 .41.057.548.436a806.05 806.05 0 0 1 1.247 3.74s.102.454-.335.454H4.673z" fill="#2E4A74"></path><path d="M18.85 14.194a1.323 1.323 0 1 1 2.648 0 1.323 1.323 0 0 1-2.647 0" fill="#E4E3D7"></path><path d="M19.513 19.485v.973c0 .924.74 1.672 1.653 1.672s1.654-.748 1.654-1.672v-.973M.992 19.485v.973c0 .924.74 1.672 1.654 1.672.913 0 1.653-.748 1.653-1.672v-.973" fill="#2A456D"></path><path d="M16.867 16.839c0 .73-.548 1.323-1.225 1.323H8.17c-.677 0-1.225-.592-1.225-1.323 0-.73.548-1.323 1.225-1.323h7.472c.677 0 1.225.592 1.225 1.323" fill="#006750"></path><path d="M11.906 14.193v-4.3H6.35c.138 1.079.939 4.3 5.555 4.3" fill="#008D70"></path><path d="M11.906 14.193v-4.3h5.555c-.139 1.079-.939 4.3-5.555 4.3" fill="#009F7F"></path><path d="M5.656 5.472l3.43 3.43h2.235L7.683 5.264H6.068c-.04 0-.25.032-.412.208M12.798 8.902h1.341l-3.638-3.638H9.16z" fill="#2A8F9E"></path></g></svg>
                            </span>
                            <div class="title">Vehicles</div>
                        </a>
                        <ul class="fs-links level2">
                            <li>
                                <a href="#" class="btn btn-link">
                                    <div class="title">Three Wheelers <span class="count">(4,409)</span></div>
                                </a>
                                <ul class="fs-links level3">
                                    <li>
                                        <a href="#" class="btn btn-link active">
                                            <div class="title">Three Wheelers</div>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="#" class="btn btn-link">
                                            <div class="title">Heavy Machinery &amp; Tractors</div>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="#" class="btn btn-link">
                                            <div class="title">Bicycles</div>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="#" class="btn btn-link">
                                            <div class="title">Boats &amp; Water Transport</div>
                                        </a>
                                    </li>
                                </ul>
                            </li>
                            <li>
                                <a href="#" class="btn btn-link">
                                    <div class="title">Heavy Machinery &amp; Tractors <span class="count">(861)</span></div>
                                </a>
                            </li>
                            <li>
                                <a href="#" class="btn btn-link">
                                    <div class="title">Bicycles <span class="count">(661)</span></div>
                                </a>
                            </li>
                            <li>
                                <a href="#" class="btn btn-link">
                                    <div class="title">Boats &amp; Water Transport <span class="count">(74)</span></div>
                                </a>
                            </li>
                        </ul>
                    </li>
                    <li>
                        <a href="#" class="btn btn-link d-flex">
                            <div class="title">Auto Parts &amp; Accessories <span class="count">(23,463)</span></div>
                        </a>
                    </li>
                    <li>
                        <a href="#" class="btn btn-link">
                            <div class="title">Cars <span class="count">(16,592)</span></div>
                        </a>
                    </li>
                    <li>
                        <a href="#" class="btn btn-link">
                            <div class="title">Motorbikes &amp; Scooters <span class="count">(16,305)</span></div>
                        </a>
                    </li>
                    <li>
                        <a href="#" class="btn btn-link">
                            <div class="title">Auto Services <span class="count">(7,577)</span></div>
                        </a>
                    </li>
                    <li>
                        <a href="#" class="btn btn-link">
                            <div class="title">Vans, Buses &amp; Lorries <span class="count">(7,083)</span></div>
                        </a>
                    </li>
                    <li>
                        <a href="#" class="btn btn-link">
                            <div class="title">Three Wheelers <span class="count">(4,409)</span></div>
                        </a>
                    </li>
                    <li>
                        <a href="#" class="btn btn-link">
                            <div class="title">Heavy Machinery &amp; Tractors <span class="count">(861)</span></div>
                        </a>
                    </li>
                    <li>
                        <a href="#" class="btn btn-link">
                            <div class="title">Bicycles <span class="count">(661)</span></div>
                        </a>
                    </li>
                    <li>
                        <a href="#" class="btn btn-link">
                            <div class="title">Boats &amp; Water Transport <span class="count">(74)</span></div>
                        </a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
    <div class="filter-section filter-location">
        <button class="btn btn-link fs-toggle" type="button" data-toggle="collapse" aria-expanded="true">
            Location <i class="fa fa-angle-down" aria-hidden="true"></i>
        </button>
        <div class="collapse show fs-collapse">
            <div class="collapse-inner">
                <ul class="fs-header">
                    <li>
                        <li>
                            <a href="#" class="btn btn-link active">
                                <div class="title">All of Sri Lanka</div>
                            </a>
                        </li>
                    </li>
                </ul>
                <ul class="fs-links level1">
                    <li>
                        <a href="#" class="btn btn-link">
                            <div class="title">Colombo <span class="count">(37,105)</span></div>
                        </a>
                    </li>
                    <li>
                        <a href="#" class="btn btn-link">
                            <div class="title">Gampaha <span class="count">(17,374)</span></div>
                        </a>
                    </li>
                    <li>
                        <a href="#" class="btn btn-link">
                            <div class="title">Kurunegala <span class="count">(5,030)</span></div>
                        </a>
                    </li>
                    <li>
                        <a href="#" class="btn btn-link">
                            <div class="title">Kalutara <span class="count">(4,330)</span></div>
                        </a>
                    </li>
                    <li>
                        <a href="#" class="btn btn-link">
                            <div class="title">Kandy <span class="count">(3,477)</span></div>
                        </a>
                    </li>
                </ul>
                <div class="collapse">
                    <ul class="fs-links level1">
                        <li>
                            <a href="#" class="btn btn-link">
                                <div class="title">Galle <span class="count">(1,561)</span></div>
                            </a>
                        </li>
                        <li>
                            <a href="#" class="btn btn-link">
                                <div class="title">Matara <span class="count">(1,398)</span></div>
                            </a>
                        </li>
                        <li>
                            <a href="#" class="btn btn-link">
                                <div class="title">Kegalle <span class="count">(1,108)</span></div>
                            </a>
                        </li>
                        <li>
                            <a href="#" class="btn btn-link">
                                <div class="title">Ratnapura <span class="count">(985)</span></div>
                            </a>
                        </li>
                        <li>
                            <a href="#" class="btn btn-link">
                                <div class="title">Puttalam <span class="count">(982)</span></div>
                            </a>
                        </li>
                        <li>
                            <a href="#" class="btn btn-link">
                                <div class="title">Anuradhapura <span class="count">(703)</span></div>
                            </a>
                        </li>
                        <li>
                            <a href="#" class="btn btn-link">
                                <div class="title">Matale <span class="count">(635)</span></div>
                            </a>
                        </li>
                        <li>
                            <a href="#" class="btn btn-link">
                                <div class="title">Hambantota <span class="count">(580)</span></div>
                            </a>
                        </li>
                        <li>
                            <a href="#" class="btn btn-link">
                                <div class="title">Badulla <span class="count">(382)</span></div>
                            </a>
                        </li>
                        <li>
                            <a href="#" class="btn btn-link">
                                <div class="title">Jaffna <span class="count">(238)</span></div>
                            </a>
                        </li>
                        <li>
                            <a href="#" class="btn btn-link">
                                <div class="title">Polonnaruwa <span class="count">(223)</span></div>
                            </a>
                        </li>
                        <li>
                            <a href="#" class="btn btn-link">
                                <div class="title">Ampara <span class="count">(217)</span></div>
                            </a>
                        </li>
                        <li>
                            <a href="#" class="btn btn-link">
                                <div class="title">Moneragala <span class="count">(196)</span></div>
                            </a>
                        </li>
                        <li>
                            <a href="#" class="btn btn-link">
                                <div class="title">Batticaloa <span class="count">(162)</span></div>
                            </a>
                        </li>
                        <li>
                            <a href="#" class="btn btn-link">
                                <div class="title">Nuwara Eliya <span class="count">(156)</span></div>
                            </a>
                        </li>
                        <li>
                            <a href="#" class="btn btn-link">
                               <div class="title">Trincomalee <span class="count">(105)</span></div>
                            </a>
                        </li>
                        <li>
                            <a href="#" class="btn btn-link">
                                <div class="title">Vavuniya <span class="count">(51)</span></div>
                            </a>
                        </li>
                        <li>
                            <a href="#" class="btn btn-link">
                                <div class="title">Kilinochchi <span class="count">(14)</span></div>
                            </a>
                        </li>
                        <li>
                            <a href="#" class="btn btn-link">
                                <div class="title">Mannar <span class="count">(9)</span></div>
                            </a>
                        </li>
                        <li>
                            <a href="#" class="btn btn-link">
                                <div class="title">Mullativu <span class="count">(4)</span></div>
                            </a>
                        </li>
                    </ul>
                </div>
                <a class="btn btn-more collapsed" data-toggle="collapse" role="button" aria-expanded="false"><span>Show more</span> <i class="fa fa-angle-double-down" aria-hidden="true"></i></a>
            </div>
        </div>
    </div>
    <div class="filter-section filter-location">
        <button class="btn btn-link fs-toggle" type="button" data-toggle="collapse" aria-expanded="true">
            Type of ad <i class="fa fa-angle-down" aria-hidden="true"></i>
        </button>
        <div class="collapse show fs-collapse">
            <div class="collapse-inner">
                <ul class="fs-links">
                    <li>
                        <div class="custom-control custom-radio">
                            <input type="radio" class="custom-control-input" name="ad_type" id="conditionRadio1">
                            <label class="custom-control-label" for="conditionRadio1">For sale <span class="count">(435)</span></label>
                        </div>
                    </li>
                    <li>
                        <div class="custom-control custom-radio">
                            <input type="radio" class="custom-control-input" name="ad_type" id="conditionRadio2">
                            <label class="custom-control-label" for="conditionRadio2">Wanted <span class="count">(435)</span></label>
                        </div>
                    </li>
                </ul>
            </div>
        </div>
    </div>
    <div class="filter-section filter-location">
        <button class="btn btn-link fs-toggle" type="button" data-toggle="collapse" aria-expanded="true">
            Condition <i class="fa fa-angle-down" aria-hidden="true"></i>
        </button>
        <div class="collapse show fs-collapse">
            <div class="collapse-inner">
                <ul class="fs-links">
                    <li>
                        <div class="custom-control custom-checkbox">
                            <input type="checkbox" class="custom-control-input" id="conditionCheck1">
                            <label class="custom-control-label" for="conditionCheck1">New <span class="count">(435)</span></label>
                        </div>
                    </li>
                    <li>
                        <div class="custom-control custom-checkbox">
                            <input type="checkbox" class="custom-control-input" id="conditionCheck2">
                            <label class="custom-control-label" for="conditionCheck2">Reconditioned <span class="count">(435)</span></label>
                        </div>
                    </li>
                    <li>
                        <div class="custom-control custom-checkbox">
                            <input type="checkbox" class="custom-control-input" id="conditionCheck3">
                            <label class="custom-control-label" for="conditionCheck3">Used <span class="count">(435)</span></label>
                        </div>
                    </li>
                </ul>
            </div>
        </div>
    </div>
    <div class="filter-section filter-location">
        <button class="btn btn-link fs-toggle" type="button" data-toggle="collapse" aria-expanded="true">
            Price (Rs) <i class="fa fa-angle-down" aria-hidden="true"></i>
        </button>
        <div class="collapse show fs-collapse">
            <div class="collapse-inner">
                <div class="fs-range">
                    <div class="row ex-small-gutters">
                        <div class="col-6">
                            <div class="form-group">
                                <input type="number" name="min_price" class="form-control" placeholder="Min">
                            </div>
                        </div>
                        <div class="col-6">
                            <div class="form-group">
                                <input type="number" name="max_price" class="form-control" placeholder="Max">
                            </div>
                        </div>
                        <div class="col-12">
                            <button type="button" class="btn btn-block btn-secondary">Apply</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
