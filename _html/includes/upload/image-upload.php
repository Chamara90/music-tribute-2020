
    <link rel="stylesheet" href="includes/upload/dist/image-uploader.min.css">
    <style>
        .step {
            font-size: 1.6em;
            font-weight: 600;
            margin-right: .5rem;
        }

        .option {
            margin-top: 2rem;
            border-bottom: 1px solid #d9d9d9;
        }

        .modal {
            position: fixed;
            top: 0;
            right: 0;
            bottom: 0;
            left: 0;
            background: rgba(0, 0, 0, .5);
            display: flex;
            align-items: center;
            justify-content: center;
        }

        .modal .content {
            background: #fff;
            display: inline-block;
            padding: 2rem;
            position: relative;
        }

        .modal .content h4 {
            margin-top: 0;
        }

        .modal .content a.close {
            position: absolute;
            top: 1rem;
            right: 1rem;
            color: inherit;
            font-size: 1.4rem;
            line-height: 1;
            font-family: 'Montserrat', sans-serif;
        }

        ::-webkit-scrollbar {
            width: 10px;
            height: 10px;
        }

        ::-webkit-scrollbar-track {
            background: transparent;
        }

        ::-webkit-scrollbar-thumb {
            background: #888;
        }

        ::-webkit-scrollbar-thumb:hover {
            background: #555;
        }

.uploaded .uploaded-image a{
     width: 15px;
     height: 15px;
     position: absolute;
     background-color: red;
}
.uploaded .uploaded-image a.up{
    width: 15px;
    height: 15px;
    right: 20px;
    top: 15px;
    cursor: pointer;
    background: url(assets/images/130906.png);
    background-size: 100%;
    opacity: 0.5;
}
.uploaded .uploaded-image a.down {
    left: auto;
    right: 20px;
    bottom: 17px;
    cursor: pointer;
    background: url(assets/images/130907.png);
    background-size: 100%;
    opacity: 0.5;
}
.image-uploader {
    min-height: 90px;
    border: none;
    
}
.image-uploader .upload-text {
    position: absolute;
    top: auto;
    right: 0;
    left: 0;
    bottom: 0;
    height: 89px;
    display: flex;
    justify-content: center;
    align-items: center;
    flex-direction: column;
    border-radius: 4px;
    background: url(assets/images/default-post-img.png);
    background-repeat: no-repeat;
    background-size: 130px;
    background-position: 7px 7px;
    border: dashed 1px #d4ded9;
    border-radius: 3px 3px 4px 4px;
}
.image-uploader .uploaded {
    padding: 0;
    padding-bottom: 90px;
    
}
.image-uploader.has-files .upload-text {
    display: block;
}
.image-uploader .upload-text i {
    font-size: 3rem;
    margin-bottom: .5rem;
    display: none;
}
.image-uploader .upload-text span {
    display: block;
    color: #fff;
    border-radius: 4px;
    background: radial-gradient(circle, #d71a21, #cb171d, #bf141a, #b31116, #a70e13);
    padding: 9px;
    border: solid 1px #ed1b24;
    cursor: pointer;
    width: 40%;
    text-align: center;
    position: absolute;
    bottom: 27px;
    left: calc(75% - 40%);
}
.image-uploader .uploaded .uploaded-image {
    display: inline-block;
    width: 100%;
    position: relative;
    margin: 0;
    background: transparent;
    cursor: default;
    border: dashed 1px #d4ded9;
    border-radius: 3px 3px 4px 4px;
    padding: 8px;
    height: 90px;
    margin-bottom: 4px;
}
.img-top {
    width: 130px;
    height: 72px;
    background-color: #e8edf0;
    position: relative;
    border-radius: 4px;
    overflow: hidden;
}
.image-uploader .uploaded .uploaded-image .delete-image {
    display: block;
        cursor: pointer;
    position: absolute;
    top: 35%;
    right: 0;
    left: 0;
    border-radius: 0;
    width: 160px;
    padding: .3rem;
    margin: auto;
    line-height: 1;
    background-color: transparent;
    -webkit-appearance: none;
    border: none;
}
.image-uploader .uploaded .uploaded-image .delete-image i {
    display: none;
}
.delete-image:before{
    content: "remove image";
    font-size: 14px;
    color: red;
    background: url(assets/images/icons8-delete-64.png);
    background-size: 13px;
    background-repeat: no-repeat;
    padding-left: 20px;
    background-position: left center;
}
.image-uploader .uploaded .uploaded-image:first-child{
        border: dashed 1px #ed1b24;
}
.image-uploader .uploaded .uploaded-image:first-child:before{
    content: "This will be your main image";
    font-size: 12px;
    color: #a8a8a8;
    font-style: italic;
    position: absolute;
    left: 146px;
    bottom: 15px;
}
</style>

<main style="margin-bottom: 2rem;">
    <div class="input-field">
        <div class="input-images-1" style="padding-top: .5rem;"></div>
    </div>
</main>



<!-- 
        <form method="POST" name="form-example-2" id="form-example-2" enctype="multipart/form-data">

            <div class="input-field">
                <label class="active">Photos</label>
                <div class="input-images-2" style="padding-top: .5rem;"></div>
            </div>

            <button>Submit and display data</button>

        </form>
 -->


<!--     <div id="show-submit-data" class="modal" style="visibility: hidden;">
        <div class="content">
            <h4>Submitted data:</h4>
            <p><strong>Uploaded images:</strong></p>
            <ul id="display-new-images"></ul>
            <p><strong>Preloaded images:</strong></p>
            <ul id="display-preloaded-images"></ul>
            <a href="javascript:$('#show-submit-data').css('visibility', 'hidden')" class="close">x</a>
        </div>
    </div> -->



