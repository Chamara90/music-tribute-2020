<link rel="icon" type="image/png" href="assets/images/favicon.png">
<link href="assets/images/apple-icon-touch.png" rel="apple-touch-icon" />
<meta name="msapplication-TileImage" content="assets/images/win8-tile-icon.png">
<link rel="stylesheet" type="text/css" href="assets/css/style.css">
