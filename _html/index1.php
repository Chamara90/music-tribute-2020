<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
    <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' name='viewport' />
    <title>Music</title>
    
    <?php include 'includes/common-doc-head.php'; ?>
    
</head>
<body>
    <?php include 'includes/page-header.php'; ?>
    <div class="container-fluid main-banner-slider">
        <div class="list-item-gallery ">
            <div class="gallery-hero owl-carousel">

                <div class="gallery-item">
                    <figure class="list-item-image">
                        <img src="assets/images/main-slide-image-1.jpg" alt="Music">
                    </figure>
                </div>

                <!-- <div class="gallery-item">
                    <figure class="list-item-image">
                    <img src="assets/images/main-slide-image-1.jpg" alt="Music">   
                    </figure>
                </div> -->
            </div>
        </div>
    </div>
    <main id="landing-page" class="main-content">

        <div class="main-content-inner">
            <div class="preloader"><span></span></div>
            <div class="page-content">
                <!-- <div class="container">
                    <div class="row">                        
                        <div class="col-lg-3 col-sm-6 col-12">
                            <div class="music-image">
                                <img src="assets/images/8.jpg" class="img-fluid" alt="img" >
                                <div class="player-container">
                                    <iframe src="https://open.spotify.com/embed/album/16rTsMjlDt6DEbLRtxvcWu" width="" height="" frameborder="0" allowtransparency="true" allow="encrypted-media"></iframe>
                                </div>
                            </div>
                        </div>

                        <div class="col-lg-3 col-sm-6 col-12">
                            <div class="music-image">
                                <img src="assets/images/8.jpg" class="img-fluid" alt="img" >
                                <div class="player-container">
                                    <iframe src="https://open.spotify.com/embed/album/1iSTXHBhLc9ImaqyvVZGft" width="" height="" frameborder="0" allowtransparency="true" allow="encrypted-media"></iframe>
                                </div>
                            </div>                            
                        </div>

                        <div class="col-lg-3 col-sm-6 col-12">
                            <div class="music-image">
                                <img src="assets/images/8.jpg" class="img-fluid" alt="img" >
                                <div class="player-container">
                                    <iframe src="https://open.spotify.com/embed/album/7aDBFWp72Pz4NZEtVBANi9" width="" height="" frameborder="0" allowtransparency="true" allow="encrypted-media"></iframe>
                                </div>
                            </div>
                        </div>

                        <div class="col-lg-3 col-sm-6 col-12">
                            <div class="music-image">
                                <img src="assets/images/8.jpg" class="img-fluid" alt="img" >
                                <div class="player-container">
                                    <iframe src="https://open.spotify.com/embed/album/1ibYM4abQtSVQFQWvDSo4J" width="" height="" frameborder="0" allowtransparency="true" allow="encrypted-media"></iframe>
                                </div>
                            </div>
                        </div>

                     </div>
                </div> -->

                <div class="container">
                    <div class="row">
                    <div class="col-lg-3 col-sm-6 col-12">
                            <div class="music-image">
                                <img src="assets/images/1.jpg" class="img-fluid" alt="img" >
                                <div class="player-container">
                                    <iframe src="https://open.spotify.com/embed/album/16rTsMjlDt6DEbLRtxvcWu" width="" height="" frameborder="0" allowtransparency="true" allow="encrypted-media"></iframe>
                                </div>
                            </div>
                        </div>

                        <div class="col-lg-3 col-sm-6 col-12">
                            <div class="music-image">
                                <img src="assets/images/2.jpg" class="img-fluid" alt="img" >
                                <div class="player-container">
                                    <iframe src="https://open.spotify.com/embed/album/1iSTXHBhLc9ImaqyvVZGft" width="" height="" frameborder="0" allowtransparency="true" allow="encrypted-media"></iframe>
                                </div>
                            </div>                            
                        </div>

                        <div class="col-lg-3 col-sm-6 col-12">
                            <div class="music-image">
                                <img src="assets/images/3.jpg" class="img-fluid" alt="img" >
                                <div class="player-container">
                                    <iframe src="https://open.spotify.com/embed/album/7aDBFWp72Pz4NZEtVBANi9" width="" height="" frameborder="0" allowtransparency="true" allow="encrypted-media"></iframe>
                                </div>
                            </div>
                        </div>

                        <div class="col-lg-3 col-sm-6 col-12">
                            <div class="music-image">
                                <img src="assets/images/4.jpg" class="img-fluid" alt="img" >
                                <div class="player-container">
                                    <iframe src="https://open.spotify.com/embed/album/1ibYM4abQtSVQFQWvDSo4J" width="" height="" frameborder="0" allowtransparency="true" allow="encrypted-media"></iframe>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-3 col-sm-6 col-12">
                            <div class="music-image">
                                <img src="assets/images/5.jpg" class="img-fluid" alt="img" >
                            </div>
                        </div>
                        <div class="col-lg-3 col-sm-6 col-12">
                            <div class="music-image">
                                <img src="assets/images/6.jpg" class="img-fluid" alt="img" >
                            </div>
                        </div>
                        <div class="col-lg-3 col-sm-6 col-12">
                            <div class="music-image">
                                <img src="assets/images/7.jpg" class="img-fluid" alt="img" >
                            </div>
                        </div>
                        <div class="col-lg-3 col-sm-6 col-12">
                            <div class="music-image">
                                <img src="assets/images/8.jpg" class="img-fluid" alt="img" >
                            </div>
                        </div>
                     </div>
                </div>


                <div class="container">
                    <div class="music-backdrop">
                        <div class="row">
                            <div class="col-lg-8">
                                <div class="backdrop-content">
                                    <iframe src="https://www.youtube.com/embed/H1Yt0xJKDY8" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                                    <a class="backdrop-btn">View More Videos</a>
                                </div>
                            </div>
                            <div class="col-lg-4">
                                <div class="backdrop-content">
                                    <img src="assets/images/9.jpg" class="img-fluid" alt="img" >
                                    <a class="backdrop-btn">View More Videos</a>
                                    <div class="player-container">
                                        <iframe src="https://open.spotify.com/embed/track/2aBxt229cbLDOvtL7Xbb9x" width="" height="" frameborder="0" allowtransparency="true" allow="encrypted-media"></iframe>
                                    </div>
                                </div>
                            </div>
                        </div>                        
                     </div>



                     








                </div>

                <!-- <div id="jquery_jplayer_1" class="jp-jplayer"></div>
                                    <div id="jp_container_1" class="jp-audio" role="application" aria-label="media player">
                                        <div class="jp-type-single">
                                            <div class="jp-gui jp-interface">
                                            <div class="jp-volume-controls">
                                                <button class="jp-mute" role="button" tabindex="0">mute</button>
                                                <button class="jp-volume-max" role="button" tabindex="0">max volume</button>
                                                <div class="jp-volume-bar">
                                                <div class="jp-volume-bar-value"></div>
                                                </div>
                                            </div>
                                            <div class="jp-controls-holder">
                                                <div class="jp-controls">
                                                <button class="jp-play" role="button" tabindex="0">play</button>
                                                <button class="jp-stop" role="button" tabindex="0">stop</button>
                                                </div>
                                                <div class="jp-progress">
                                                <div class="jp-seek-bar">
                                                    <div class="jp-play-bar"></div>
                                                </div>
                                                </div>
                                                <div class="jp-current-time" role="timer" aria-label="time">&nbsp;</div>
                                                <div class="jp-duration" role="timer" aria-label="duration">&nbsp;</div>
                                                <div class="jp-toggles">
                                                <button class="jp-repeat" role="button" tabindex="0">repeat</button>
                                                </div>
                                            </div>
                                            </div>
                                            <div class="jp-details">
                                            <div class="jp-title" aria-label="title">&nbsp;</div>
                                            <span class="jp-artist" aria-label="artist">&nbsp;</span>
                                            </div>
                                            <div class="jp-no-solution">
                                            <span>Update Required</span>
                                            To play the media you will need to either update your browser to a recent version or update your <a href="http://get.adobe.com/flashplayer/" target="_blank">Flash plugin</a>.
                                            </div>
                                        </div>
                                    </div>
                                </div> -->

                <!-- <div class="container">
                    <div id="jquery_jplayer_1" class="jp-jplayer"></div>
                    <div id="jp_container_1" class="jp-audio">
                        <div class="jp-type-playlist">
                            <div class="jp-gui jp-interface">
                                <ul class="jp-controls">
                                    <li><a href="javascript:;" class="jp-previous" tabindex="1"></a></li>
                                    <li><a href="javascript:;" class="jp-play" tabindex="1"></a></li>
                                    <li><a href="javascript:;" class="jp-pause" tabindex="1"></a></li>
                                    <li><a href="javascript:;" class="jp-next" tabindex="1"></a></li>
                                    <li><a href="javascript:;" class="jp-stop" tabindex="1"></a></li>
                                    <li><a href="javascript:;" class="jp-mute" tabindex="1" title="mute">mute</a></li>
                                    <li><a href="javascript:;" class="jp-unmute" tabindex="1" title="unmute">unmute</a></li>
                                    <li><a href="javascript:;" class="jp-volume-max" tabindex="1" title="max volume">max volume</a></li>
                                </ul>
                                <div class="jp-progress">
                                    <div class="jp-seek-bar">
                                        <div class="jp-play-bar"></div>
                                    </div>
                                </div>
                                <div class="jp-volume-bar">
                                    <div class="jp-volume-bar-value"></div>
                                </div>
                                <div class="jp-current-time"></div>
                                <div class="jp-duration"></div>
                                <ul class="jp-toggles">
                                    <li><a href="javascript:;" class="jp-shuffle" tabindex="1" title="shuffle">shuffle</a></li>
                                    <li><a href="javascript:;" class="jp-shuffle-off" tabindex="1" title="shuffle off">shuffle off</a></li>
                                    <li><a href="javascript:;" class="jp-repeat" tabindex="1" title="repeat">repeat</a></li>
                                    <li><a href="javascript:;" class="jp-repeat-off" tabindex="1" title="repeat off">repeat off</a></li>
                                </ul>
                            </div>
                                <div class="jp-playlist">
                                <ul>
                                    <li></li>
                                </ul>
                            </div>                
                         </div>
                    </div>
                 </div> -->
    
        
                                            
        

        
    </main>
    <?php include 'includes/page-footer.php'; ?>
    <?php include 'includes/common-scripts.php'; ?>

    <script type="text/javascript">
        (function($) {
            "use strict";

            $('.gallery-hero').owlCarousel({
                margin: 15,
                nav: true,
                dots: true,
                items: 1,
                smartSpeed: 600,
                responsive:{
                    420: {
                        items: 1,
                    },
                    576: {
                        items: 1,
                    },
                    992: {
                        margin: 30,
                        items: 1,
                    }
                }
            });

        })(jQuery);
    </script>
    
    <script type="text/javascript" src="assets/js/libs/jPlayer/jquery.jplayer.min.js"></script>
    <script type="text/javascript" src="assets/js/libs/jPlayer/playlist/jplayer.playlist.min.js"></script>
    <script type="text/javascript">
    // $(document).ready(function(){
    //   $("#jquery_jplayer_1").jPlayer({

    //     ready: function () {
    //       $(this).jPlayer("setMedia", {
    //         title: "Bubble Song",
    //         mp3: "https://www.soundhelix.com/examples/mp3/SoundHelix-Song-1.mp3",
    //         artist: "The Stark Palace"
    //       });
    //     },

    //     cssSelectorAncestor: "#jp_container_1",
    //     swfPath: "/js",
    //     // supplied: "m4a, oga",
    //     supplied: "mp3",
    //     useStateClassSkin: true,
    //     autoBlur: false,
    //     smoothPlayBar: true,
    //     keyEnabled: true,
    //     remainingDuration: true,
    //     toggleDuration: true
    //   });
    // });
  </script>

<script>

// $(document).ready(function(){
// new jPlayerPlaylist({
//     jPlayer: "#jquery_jplayer_1",
//     cssSelectorAncestor: "#jp_container_1"
//     }, [
//           {
//             title:"Cro Magnon Man",
//             mp3:"http://www.jplayer.org/audio/mp3/TSP-01-Cro_magnon_man.mp3",
//             oga:"http://www.jplayer.org/audio/ogg/TSP-01-Cro_magnon_man.ogg"
//           },
//           {
//             title:"Your Face",
//             mp3:"http://www.jplayer.org/audio/mp3/TSP-05-Your_face.mp3",
//             oga:"http://www.jplayer.org/audio/ogg/TSP-05-Your_face.ogg"
//           },
//     ],{
//         supplied: "mp3",
//         wmode: "window",
//         smoothPlayBar: true,
//         keyEnabled: true
//     });
//     $("#jplayer_inspector_1").jPlayerInspector({jPlayer:$("#jquery_jplayer_1")});
// });

   
</script>

    

</body>
</html>
