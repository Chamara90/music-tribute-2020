<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
    <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' name='viewport' />
    <title>Music</title>
    
    <?php include 'includes/common-doc-head.php'; ?>
    <style>
        .nav-tabs {
            border-bottom: none;
            width: 100px;
            margin: 0 auto;
            margin-top: 1rem;
        }

        .nav-tabs .nav-link {
            color: #000;
            background-color: #fff;
            border-radius: 0;
            border-color: #ccc;
        }
        .nav-link .active {
            color: red;
            background-color: blue;
            border-color: #000;
        }
        
        .nav-tabs .nav-link:hover {
            border-color: #ccc;
            background-color: #ccc;
            border-color: #ccc;
        }

        .nav-tabs .nav-link.active, .nav-tabs .nav-item.show .nav-link {
            color: #fff;
            background-color: #000;
            border-color: #dee2e6 #dee2e6 #fff;
        }
        
    </style>
</head>
<body>
    <?php include 'includes/page-header.php'; ?>

    <main id="landing-page" class="main-content">

        <div class="main-content-inner">
            <div class="preloader"><span></span></div>
            <div class="page-content">

               <!-- <div class="container pt-4">
                    <div class="row">
                        
                        
                        
                        <div class="col-lg-3 col-sm-6 col-12">
                        <div class="music-image">
                            <img src="assets/images/writer-image/writer-image-1.jpg" class="img-fluid" alt="img" >
                            <div class="player-container">
                                <div class="example">
                                    <ul class="playlist" style="display: none;">
                                        <li data-cover="assets/images/writer-image/writer-image-1.jpg" data-artist="Nisanga Mayadunne"><a
                                                href="assets/songs/aparimitha_wu.mp3">Aparimitha Wu</a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-lg-3 col-sm-6 col-12">
                        <div class="music-image">
                            <img src="assets/images/writer-image/writer-image-2.jpg" class="img-fluid" alt="img" >
                            <div class="player-container">
                                <div class="example">
                                    <ul class="playlist" style="display: none;">
                                        <li data-cover="assets/images/writer-image/writer-image-2.jpg" data-artist="Nisanga Mayadunne"><a
                                                href="assets/songs/karuna_meth_mudhitha.mp3">Karuna Meth Mudhitha</a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3 col-sm-6 col-12">
                        <div class="music-image">
                            <img src="assets/images/writer-image/writer-image-5.jpg" class="img-fluid" alt="img" >
                            <div class="player-container">
                                <div class="example">
                                    <ul class="playlist" style="display: none;">
                                        <li data-cover="assets/images/writer-image/writer-image-5.jpg" data-artist="Nisanga Mayadunne"><a
                                                href="assets/songs/manamaliya_wee.mp3">Manamaliya Wee</a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3 col-sm-6 col-12">
                        <div class="music-image">
                            <img src="assets/images/writer-image/writer-image-4.JPG" class="img-fluid" alt="img" >
                            <div class="player-container">
                                <div class="example">
                                    <ul class="playlist" style="display: none;">
                                        <li data-cover="assets/images/writer-image/writer-image-4.JPG" data-artist="Nisanga Mayadunne"><a
                                                href="assets/songs/nuba_lagin.mp3">Nuba Lagin</a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                        
                    <div class="col-lg-3 col-sm-6 col-12">
                        <div class="music-image">
                            <img src="assets/images/writer-image/writer-image-3.JPG" class="img-fluid" alt="img" >
                            <div class="player-container">
                                <div class="example">
                                    <ul class="playlist" style="display: none;">
                                        <li data-cover="assets/images/writer-image/writer-image-3.JPG" data-artist="Nisanga Mayadunne"><a
                                                href="assets/songs/pipi_mal_gomuwe.mp3">Pipi Mal Gomuwe</a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                        
                    <div class="col-lg-3 col-sm-6 col-12">
                        <div class="music-image">
                            <img src="assets/images/writer-image/writer-image-4.JPG" class="img-fluid" alt="img" >
                            <div class="player-container">
                                <div class="example">
                                    <ul class="playlist" style="display: none;">
                                        <li data-cover="assets/images/writer-image/writer-image-4.JPG" data-artist="Nisanga Mayadunne"><a
                                                href="assets/songs/piyanane.mp3">Piyanane</a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                        
                    <div class="col-lg-3 col-sm-6 col-12">
                        <div class="music-image">
                            <img src="assets/images/writer-image/writer-image-2.jpg" class="img-fluid" alt="img" >
                            <div class="player-container">
                                <div class="example">
                                    <ul class="playlist" style="display: none;">
                                        <li data-cover="assets/images/writer-image/writer-image-2.jpg" data-artist="Nisanga Mayadunne"><a
                                                href="assets/songs/sadak_lesin_paya.mp3">Sadak Lesin Paya</a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                        
                    <div class="col-lg-3 col-sm-6 col-12">
                        <div class="music-image">
                            <img src="assets/images/writer-image/writer-image-3.JPG" class="img-fluid" alt="img" >
                            <div class="player-container">
                                <div class="example">
                                    <ul class="playlist" style="display: none;">
                                        <li data-cover="assets/images/writer-image/writer-image-3.JPG" data-artist="Nisanga Mayadunne"><a
                                                href="assets/songs/sande_sisilath.mp3">Sade Siilath</a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                        
                        
                        
                        
                        
                    
                    <div class="col-lg-3 col-sm-6 col-12">
                                    <div class="music-image">
                                        <img src="assets/images/writer-image/writer-image-1.jpg" class="img-fluid" alt="img" >
                                        <div class="player-container">
                                            <div class="example">
                                                <ul class="playlist" style="display: none;">
                                                    <li data-cover="assets/images/writer-image/writer-image-1.jpg" data-artist="Nisanga Mayadunne"><a
                                                            href="assets/songs/dethol_wila_matha.mp3">Dethol Wila Matha</a></li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                        
                                <div class="col-lg-3 col-sm-6 col-12">
                                    <div class="music-image">
                                        <img src="assets/images/writer-image/writer-image-2.jpg" class="img-fluid" alt="img" >
                                        <div class="player-container">
                                            <div class="example">
                                                <ul class="playlist" style="display: none;">
                                                    <li data-cover="assets/images/writer-image/writer-image-2.jpg" data-artist="Nisanga Mayadunne"><a
                                                            href="assets/songs/dolos_mahe_sanda.mp3">Dolos Mahe Sanda</a></li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                        
                                <div class="col-lg-3 col-sm-6 col-12">
                                    <div class="music-image">
                                        <img src="assets/images/writer-image/writer-image-3.JPG" class="img-fluid" alt="img" >
                                        <div class="player-container">
                                            <div class="example">
                                                <ul class="playlist" style="display: none;">
                                                    <li data-cover="assets/images/writer-image/writer-image-3.JPG" data-artist="Nisanga Mayadunne"><a
                                                            href="assets/songs/langin_hitiyath.mp3">Lagin Hitiyath</a></li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                        
                                <div class="col-lg-3 col-sm-6 col-12">
                                    <div class="music-image">
                                        <img src="assets/images/writer-image/writer-image-4.JPG" class="img-fluid" alt="img" >
                                        <div class="player-container">
                                            <div class="example">
                                                <ul class="playlist" style="display: none;">
                                                    <li data-cover="assets/images/writer-image/writer-image-4.JPG" data-artist="Nisanga Mayadunne"><a
                                                            href="assets/songs/mal_hasarel.mp3">Mal Hasarel</a></li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </div> 

                                <div class="col-lg-3 col-sm-6 col-12">
                                    <div class="music-image">
                                        <img src="assets/images/writer-image/writer-image-5.jpg" class="img-fluid" alt="img" >
                                        <div class="player-container">
                                            <div class="example">
                                                <ul class="playlist" style="display: none;">
                                                    <li data-cover="assets/images/writer-image/writer-image-5.jpg" data-artist="Nisanga Mayadunne"><a
                                                            href="assets/songs/nuba_awidin.mp3">Nuba Awidin</a></li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                        
                        
                                <div class="col-lg-3 col-sm-6 col-12">
                                    <div class="music-image">
                                        <img src="assets/images/writer-image/writer-image-1.jpg" class="img-fluid" alt="img" >
                                        <div class="player-container">
                                            <div class="example">
                                                <ul class="playlist" style="display: none;">
                                                    <li data-cover="assets/images/writer-image/writer-image-1.jpg" data-artist="Nisanga Mayadunne"><a
                                                            href="assets/songs/nuba_kohedai.MP3">Nuba Kohedai</a></li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                 <div class="col-lg-3 col-sm-6 col-12">
                                    <div class="music-image">
                                        <img src="assets/images/writer-image/writer-image-2.jpg" class="img-fluid" alt="img" >
                                        <div class="player-container">
                                            <div class="example">
                                                <ul class="playlist" style="display: none;">
                                                    <li data-cover="assets/images/writer-image/writer-image-2.jpg" data-artist="Nisanga Mayadunne"><a
                                                            href="assets/songs/oba_paewa_sina.mp3">Oba Peewa Sina</a></li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                        
                                <div class="col-lg-3 col-sm-6 col-12">
                                    <div class="music-image">
                                        <img src="assets/images/writer-image/writer-image-4.JPG" class="img-fluid" alt="img" >
                                        <div class="player-container">
                                            <div class="example">
                                                <ul class="playlist" style="display: none;">
                                                    <li data-cover="assets/images/writer-image/writer-image-4.JPG" data-artist="Nisanga Mayadunne"><a
                                                            href="assets/songs/pinna_wetenaa.mp3">Pinna Wetenaa</a></li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                    <div class="col-lg-3 col-sm-6 col-12">
                                    <div class="music-image">
                                        <img src="assets/images/writer-image/writer-image-3.JPG" class="img-fluid" alt="img" >
                                        <div class="player-container">
                                            <div class="example">
                                                <ul class="playlist" style="display: none;">
                                                    <li data-cover="assets/images/writer-image/writer-image-3.JPG" data-artist="Nisanga Mayadunne"><a
                                                            href="assets/songs/sansaraye.mp3">Sansaraye</a></li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                        
                                <div class="col-lg-3 col-sm-6 col-12">
                                    <div class="music-image">
                                        <img src="assets/images/writer-image/writer-image-5.jpg" class="img-fluid" alt="img" >
                                        <div class="player-container">
                                            <div class="example">
                                                <ul class="playlist" style="display: none;">
                                                    <li data-cover="assets/images/writer-image/writer-image-5.jpg" data-artist="Nisanga Mayadunne"><a
                                                            href="assets/songs/sithe_siraunu.mp3">Sithe Siraunu</a></li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                        
                                <div class="col-lg-3 col-sm-6 col-12">
                                    <div class="music-image">
                                        <img src="assets/images/writer-image/writer-image-1.jpg" class="img-fluid" alt="img" >
                                        <div class="player-container">
                                            <div class="example">
                                                <ul class="playlist" style="display: none;">
                                                    <li data-cover="assets/images/writer-image/writer-image-1.jpg" data-artist="Nisanga Mayadunne"><a
                                                            href="assets/songs/yanna_denna_mata.mp3">Yanna Denna Mata</a></li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </div>
 
                     </div>
                </div> -->
                
                <div class="container">
                    <div class="tab-content">
                        
                        <div class="tab-pane active" id="tabs-1" role="tabpanel">
                            <div class="container">
                            <div class="row">
                                 <div class="col-lg-3 col-sm-6 col-12">
                                    <div class="music-image">
                                        <img src="assets/images/writer-image/writer-image-1.jpg" class="img-fluid" alt="img" >
                                        <div class="player-container">
                                            <div class="example">
                                                <ul class="playlist" style="display: none;">
                                                    <li data-cover="assets/images/writer-image/writer-image-1.jpg" data-artist="Nisanga Mayadunne"><a
                                                            href="assets/songs/aparimitha_wu.mp3">Aparimitha Wu</a></li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-lg-3 col-sm-6 col-12">
                                    <div class="music-image">
                                        <img src="assets/images/writer-image/writer-image-2.jpg" class="img-fluid" alt="img" >
                                        <div class="player-container">
                                            <div class="example">
                                                <ul class="playlist" style="display: none;">
                                                    <li data-cover="assets/images/writer-image/writer-image-2.jpg" data-artist="Nisanga Mayadunne"><a
                                                            href="assets/songs/karuna_meth_mudhitha.mp3">Karuna Meth Mudhitha</a></li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-3 col-sm-6 col-12">
                                    <div class="music-image">
                                        <img src="assets/images/writer-image/writer-image-5.jpg" class="img-fluid" alt="img" >
                                        <div class="player-container">
                                            <div class="example">
                                                <ul class="playlist" style="display: none;">
                                                    <li data-cover="assets/images/writer-image/writer-image-5.jpg" data-artist="Nisanga Mayadunne"><a
                                                            href="assets/songs/manamaliya_wee.mp3">Manamaliya Wee</a></li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-3 col-sm-6 col-12">
                                    <div class="music-image">
                                        <img src="assets/images/writer-image/writer-image-4.JPG" class="img-fluid" alt="img" >
                                        <div class="player-container">
                                            <div class="example">
                                                <ul class="playlist" style="display: none;">
                                                    <li data-cover="assets/images/writer-image/writer-image-4.JPG" data-artist="Nisanga Mayadunne"><a
                                                            href="assets/songs/nuba_lagin.mp3">Nuba Lagin</a></li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-lg-3 col-sm-6 col-12">
                                    <div class="music-image">
                                        <img src="assets/images/writer-image/writer-image-3.JPG" class="img-fluid" alt="img" >
                                        <div class="player-container">
                                            <div class="example">
                                                <ul class="playlist" style="display: none;">
                                                    <li data-cover="assets/images/writer-image/writer-image-3.JPG" data-artist="Nisanga Mayadunne"><a
                                                            href="assets/songs/pipi_mal_gomuwe.mp3">Pipi Mal Gomuwe</a></li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-lg-3 col-sm-6 col-12">
                                    <div class="music-image">
                                        <img src="assets/images/writer-image/writer-image-4.JPG" class="img-fluid" alt="img" >
                                        <div class="player-container">
                                            <div class="example">
                                                <ul class="playlist" style="display: none;">
                                                    <li data-cover="assets/images/writer-image/writer-image-4.JPG" data-artist="Nisanga Mayadunne"><a
                                                            href="assets/songs/piyanane.mp3">Piyanane</a></li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-lg-3 col-sm-6 col-12">
                                    <div class="music-image">
                                        <img src="assets/images/writer-image/writer-image-2.jpg" class="img-fluid" alt="img" >
                                        <div class="player-container">
                                            <div class="example">
                                                <ul class="playlist" style="display: none;">
                                                    <li data-cover="assets/images/writer-image/writer-image-2.jpg" data-artist="Nisanga Mayadunne"><a
                                                            href="assets/songs/sadak_lesin_paya.mp3">Sadak Lesin Paya</a></li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-lg-3 col-sm-6 col-12">
                                    <div class="music-image">
                                        <img src="assets/images/writer-image/writer-image-3.JPG" class="img-fluid" alt="img" >
                                        <div class="player-container">
                                            <div class="example">
                                                <ul class="playlist" style="display: none;">
                                                    <li data-cover="assets/images/writer-image/writer-image-3.JPG" data-artist="Nisanga Mayadunne"><a
                                                            href="assets/songs/sande_sisilath.mp3">Sade Siilath</a></li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            </div>
                        </div>
                        
                        <div class="tab-pane" id="tabs-2" role="tabpanel">
                            <div class="container">
                            <div class="row">
                                <div class="col-lg-3 col-sm-6 col-12">
                                    <div class="music-image">
                                        <img src="assets/images/writer-image/writer-image-1.jpg" class="img-fluid" alt="img" >
                                        <div class="player-container">
                                            <div class="example">
                                                <ul class="playlist" style="display: none;">
                                                    <li data-cover="assets/images/writer-image/writer-image-1.jpg" data-artist="Nisanga Mayadunne"><a
                                                            href="assets/songs/dethol_wila_matha.mp3">Dethol Wila Matha</a></li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                        
                                <div class="col-lg-3 col-sm-6 col-12">
                                    <div class="music-image">
                                        <img src="assets/images/writer-image/writer-image-2.jpg" class="img-fluid" alt="img" >
                                        <div class="player-container">
                                            <div class="example">
                                                <ul class="playlist" style="display: none;">
                                                    <li data-cover="assets/images/writer-image/writer-image-2.jpg" data-artist="Nisanga Mayadunne"><a
                                                            href="assets/songs/dolos_mahe_sanda.mp3">Dolos Mahe Sanda</a></li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                        
                                <div class="col-lg-3 col-sm-6 col-12">
                                    <div class="music-image">
                                        <img src="assets/images/writer-image/writer-image-3.JPG" class="img-fluid" alt="img" >
                                        <div class="player-container">
                                            <div class="example">
                                                <ul class="playlist" style="display: none;">
                                                    <li data-cover="assets/images/writer-image/writer-image-3.JPG" data-artist="Nisanga Mayadunne"><a
                                                            href="assets/songs/langin_hitiyath.mp3">Lagin Hitiyath</a></li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                        
                                <div class="col-lg-3 col-sm-6 col-12">
                                    <div class="music-image">
                                        <img src="assets/images/writer-image/writer-image-4.JPG" class="img-fluid" alt="img" >
                                        <div class="player-container">
                                            <div class="example">
                                                <ul class="playlist" style="display: none;">
                                                    <li data-cover="assets/images/writer-image/writer-image-4.JPG" data-artist="Nisanga Mayadunne"><a
                                                            href="assets/songs/mal_hasarel.mp3">Mal Hasarel</a></li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </div> 

                                <div class="col-lg-3 col-sm-6 col-12">
                                    <div class="music-image">
                                        <img src="assets/images/writer-image/writer-image-5.jpg" class="img-fluid" alt="img" >
                                        <div class="player-container">
                                            <div class="example">
                                                <ul class="playlist" style="display: none;">
                                                    <li data-cover="assets/images/writer-image/writer-image-5.jpg" data-artist="Nisanga Mayadunne"><a
                                                            href="assets/songs/nuba_awidin.mp3">Nuba Awidin</a></li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                        
                        
                                <div class="col-lg-3 col-sm-6 col-12">
                                    <div class="music-image">
                                        <img src="assets/images/writer-image/writer-image-1.jpg" class="img-fluid" alt="img" >
                                        <div class="player-container">
                                            <div class="example">
                                                <ul class="playlist" style="display: none;">
                                                    <li data-cover="assets/images/writer-image/writer-image-1.jpg" data-artist="Nisanga Mayadunne"><a
                                                            href="assets/songs/nuba_kohedai.MP3">Nuba Kohedai</a></li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                 <div class="col-lg-3 col-sm-6 col-12">
                                    <div class="music-image">
                                        <img src="assets/images/writer-image/writer-image-2.jpg" class="img-fluid" alt="img" >
                                        <div class="player-container">
                                            <div class="example">
                                                <ul class="playlist" style="display: none;">
                                                    <li data-cover="assets/images/writer-image/writer-image-2.jpg" data-artist="Nisanga Mayadunne"><a
                                                            href="assets/songs/oba_paewa_sina.mp3">Oba Peewa Sina</a></li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                        
                                <div class="col-lg-3 col-sm-6 col-12">
                                    <div class="music-image">
                                        <img src="assets/images/writer-image/writer-image-4.JPG" class="img-fluid" alt="img" >
                                        <div class="player-container">
                                            <div class="example">
                                                <ul class="playlist" style="display: none;">
                                                    <li data-cover="assets/images/writer-image/writer-image-4.JPG" data-artist="Nisanga Mayadunne"><a
                                                            href="assets/songs/pinna_wetenaa.mp3">Pinna Wetenaa</a></li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                    <div class="col-lg-3 col-sm-6 col-12">
                                    <div class="music-image">
                                        <img src="assets/images/writer-image/writer-image-3.JPG" class="img-fluid" alt="img" >
                                        <div class="player-container">
                                            <div class="example">
                                                <ul class="playlist" style="display: none;">
                                                    <li data-cover="assets/images/writer-image/writer-image-3.JPG" data-artist="Nisanga Mayadunne"><a
                                                            href="assets/songs/sansaraye.mp3">Sansaraye</a></li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                        
                                <div class="col-lg-3 col-sm-6 col-12">
                                    <div class="music-image">
                                        <img src="assets/images/writer-image/writer-image-5.jpg" class="img-fluid" alt="img" >
                                        <div class="player-container">
                                            <div class="example">
                                                <ul class="playlist" style="display: none;">
                                                    <li data-cover="assets/images/writer-image/writer-image-5.jpg" data-artist="Nisanga Mayadunne"><a
                                                            href="assets/songs/sithe_siraunu.mp3">Sithe Siraunu</a></li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                        
                                <div class="col-lg-3 col-sm-6 col-12">
                                    <div class="music-image">
                                        <img src="assets/images/writer-image/writer-image-1.jpg" class="img-fluid" alt="img" >
                                        <div class="player-container">
                                            <div class="example">
                                                <ul class="playlist" style="display: none;">
                                                    <li data-cover="assets/images/writer-image/writer-image-1.jpg" data-artist="Nisanga Mayadunne"><a
                                                            href="assets/songs/yanna_denna_mata.mp3">Yanna Denna Mata</a></li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        </div>
                        
                    </div>
                    <ul class="nav nav-tabs" role="tablist">
                        <li class="nav-item">
                            <a class="nav-link active" data-toggle="tab" href="#tabs-1" role="tab">1</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" data-toggle="tab" href="#tabs-2" role="tab">2</a>
                        </li>
                    </ul><!-- Tab panes -->
                    
                    
                
                </div>
   
    </main>
    <?php include 'includes/page-footer.php'; ?>
    <?php include 'includes/common-scripts.php'; ?>

<!--
    <script type="text/javascript">
        (function($) {
            "use strict";

            $('.gallery-hero').owlCarousel({
                margin: 15,
                nav: true,
                dots: true,
                items: 1,
                smartSpeed: 600,
                responsive:{
                    420: {
                        items: 1,
                    },
                    576: {
                        items: 1,
                    },
                    992: {
                        margin: 30,
                        items: 1,
                    }
                }
            });

        })(jQuery);
    </script>
-->
    
    <script type="text/javascript" src="assets/js/libs/jPlayer/jquery.jplayer.min.js"></script>
    <script type="text/javascript" src="assets/js/libs/jPlayer/playlist/jplayer.playlist.min.js"></script>
    <?php include 'audioplyer.php'; ?>        
    <script type="text/javascript">
    // $(document).ready(function(){
    //   $("#jquery_jplayer_1").jPlayer({

    //     ready: function () {
    //       $(this).jPlayer("setMedia", {
    //         title: "Bubble Song",
    //         mp3: "https://www.soundhelix.com/examples/mp3/SoundHelix-Song-1.mp3",
    //         artist: "The Stark Palace"
    //       });
    //     },

    //     cssSelectorAncestor: "#jp_container_1",
    //     swfPath: "/js",
    //     // supplied: "m4a, oga",
    //     supplied: "mp3",
    //     useStateClassSkin: true,
    //     autoBlur: false,
    //     smoothPlayBar: true,
    //     keyEnabled: true,
    //     remainingDuration: true,
    //     toggleDuration: true
    //   });
    // });
  </script>

<script>

// $(document).ready(function(){
// new jPlayerPlaylist({
//     jPlayer: "#jquery_jplayer_1",
//     cssSelectorAncestor: "#jp_container_1"
//     }, [
//           {
//             title:"Cro Magnon Man",
//             mp3:"http://www.jplayer.org/audio/mp3/TSP-01-Cro_magnon_man.mp3",
//             oga:"http://www.jplayer.org/audio/ogg/TSP-01-Cro_magnon_man.ogg"
//           },
//           {
//             title:"Your Face",
//             mp3:"http://www.jplayer.org/audio/mp3/TSP-05-Your_face.mp3",
//             oga:"http://www.jplayer.org/audio/ogg/TSP-05-Your_face.ogg"
//           },
//     ],{
//         supplied: "mp3",
//         wmode: "window",
//         smoothPlayBar: true,
//         keyEnabled: true
//     });
//     $("#jplayer_inspector_1").jPlayerInspector({jPlayer:$("#jquery_jplayer_1")});
// });

   
</script>

    

</body>
</html>
