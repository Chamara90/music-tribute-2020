<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
    <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' name='viewport' />
    <title>Music</title>
    <?php include 'includes/common-doc-head.php'; ?>
    <style>
        .interview-video {
            margin-bottom: 1rem;
        }
        .interview-video p {
            height: 38px;
        }
        .nav-tabs {
            border-bottom: none;
            width: 100px;
            margin: 0 auto;
            margin-top: 1rem;
        }

        .nav-tabs .nav-link {
            color: #000;
            background-color: #fff;
            border-radius: 0;
            border-color: #ccc;
        }
        .nav-link .active {
            color: red;
            background-color: blue;
            border-color: #000;
        }
        
        .nav-tabs .nav-link:hover {
            border-color: #ccc;
            background-color: #ccc;
            border-color: #ccc;
        }

        .nav-tabs .nav-link.active, .nav-tabs .nav-item.show .nav-link {
            color: #fff;
            background-color: #000;
            border-color: #dee2e6 #dee2e6 #fff;
        }
    </style>
</head>
<body>
    <?php include 'includes/page-header.php'; ?>

    <main id="landing-page" class="main-content pt-0">
        <div class="container position">
            <div class="list-item-gallery ">
                  <div class="top-main-banner w-75">
                      <img src="assets/images/filte1.jpg" alt="Music">
                  </div>
            </div>
            <div class="main-banner">
                <div class="gray-blure"></div>
            </div>
        </div>
        <div class="container">
            <nav class="navbar navbar-expand-lg navbar-light w-75 mx-auto p-0  mt-2">

              <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
              </button>

              <div class="collapse navbar-collapse row d-flex justify-content-end no-gutters" id="navbarSupportedContent">
                <ul class="navbar-nav col-12 col-sm-5">
                  <li class="nav-item active">
                    <a class="nav-link" href="#">All <span class="sr-only">(current)</span></a>
                  </li>
                  <li class="nav-item">
                    <a class="nav-link" href="#">Official</a>
                  </li>
                  <li class="nav-item">
                    <a class="nav-link" href="#">Interviews</a>
                  </li>
                  <li class="nav-item">
                    <a class="nav-link" href="#">Short Films</a>
                  </li>
                </ul>
                <form class="col-12 col-sm-5">
                    <div class="">
                        <div class="input-group">
                            
                            <input type="text" class="form-control" name="searchtext" placeholder="SEARCH MOVIES & TV">
                            <div class="input-group-append">
                                <button type="submit" class="btn btn-search">
                                    <i aria-hidden="true"><svg version="1.1" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="16px" height="16px" viewBox="0 0 16 16">
                                    <path d="M15.561,13.438l-3.672-3.67c-0.546,0.85-1.27,1.574-2.119,2.121l3.67,3.671c0.586,0.586,1.536,0.586,2.121,0 C16.146,14.975,16.146,14.025,15.561,13.438"></path>
                                    <path d="M11.999,6c0-3.313-2.686-6-5.999-6C2.686,0,0,2.687,0,6s2.686,5.999,6,5.999 C9.313,11.999,11.999,9.313,11.999,6 M6,10.499c-2.481,0-4.5-2.018-4.5-4.499S3.519,1.5,6,1.5s4.5,2.019,4.5,4.5 S8.481,10.499,6,10.499"></path></svg></i>
                                </button>
                            </div>
                        </div>
                    </div>
                </form>
              </div>
            </nav>
        </div>
        <div class="main-content-inner">
            <div class="preloader"><span></span></div>
            <div class="page-content">
                <br>
                <br>
               <!-- <div class="container pt-4">
                    <div class="row">
                       
                        
                        <div class="col-lg-4 col-sm-6 col-12">
                            <div class="interview-video">
                                <iframe style="width:100%;height:220px;margin-bottom:1rem;" src="https://www.youtube.com/embed/WWLP9QypdQc" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                                <p>Interview with Ranjith Pandithage</p>
                            </div>
                        </div>

                        <div class="col-lg-4 col-sm-6 col-12">
                            <div class="interview-video">
                                <iframe style="width:100%;height:220px;margin-bottom:1rem;" src="https://www.youtube.com/embed/lYJUW5UGjYo" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                                <p>Interview with Kishu Gomes on Buisness Today</p>
                            </div>
                        </div>

                        <div class="col-lg-4 col-sm-6 col-12">
                            <div class="interview-video">
                                <iframe style="width:100%;height:220px;margin-bottom:1rem;" src="https://www.youtube.com/embed/3fP-K487tX0" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                                <p>Interview with Ramani Fernando on Business Today</p>
                            </div>
                        </div>

                        <div class="col-lg-4 col-sm-6 col-12">
                            <div class="interview-video">
                                <iframe style="width:100%;height:220px;margin-bottom:1rem;" src="https://www.youtube.com/embed/PJMKpqqd0fk" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                                <p>Interview with Ajith Gunawardena on Business Today</p>
                            </div>
                        </div>


                        <div class="col-lg-4 col-sm-6 col-12">
                            <div class="interview-video">
                                <iframe style="width:100%;height:220px;margin-bottom:1rem;" src="https://www.youtube.com/embed/gMb9uH2G2Zc" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                                <p>Interview with Rohan Petiyagoda on Business Today</p>
                            </div>
                        </div>

                        <div class="col-lg-4 col-sm-6 col-12">
                            <div class="interview-video">
                                <iframe style="width:100%;height:220px;margin-bottom:1rem;" src="https://www.youtube.com/embed/SE-mMLMkqxM" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                                <p>Interview with Bramanage Premalal on Business Today</p>
                            </div>
                        </div>

                        <div class="col-lg-4 col-sm-6 col-12">
                            <div class="interview-video">
                                <iframe style="width:100%;height:220px;margin-bottom:1rem;" src="https://www.youtube.com/embed/pCG2UXlO2v8" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                                <p>Interview with Dr. Harsha Cabral on Business Today</p>
                            </div>
                        </div>

                        <div class="col-lg-4 col-sm-6 col-12">
                            <div class="interview-video">
                                <iframe style="width:100%;height:220px;margin-bottom:1rem;" src="https://www.youtube.com/embed/FAzFVidCyVs" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                                <p>Interview with Channa De Silva on Business Today</p>
                            </div>
                        </div>

                        <div class="col-lg-4 col-sm-6 col-12">
                            <div class="interview-video">
                                <iframe style="width:100%;height:220px;margin-bottom:1rem;" src="https://www.youtube.com/embed/vCIwt3spJgE" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                                <p>Interview with Dayan Gomes on Business Today</p>
                            </div>
                        </div>

                                       
                    
                    <div id="show-more-container">
                        
                        
                        <div class="container pt-4">
                            <div class="row">
                                
                                <div class="col-lg-4 col-sm-6 col-12">
                            <div class="interview-video">
                                <iframe style="width:100%;height:220px;margin-bottom:1rem;" src="https://www.youtube.com/embed/wKvxOQRhKq8" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                                <p>Interview with Eric Sooriyasena on Business Today</p>
                            </div>
                        </div>

                        <div class="col-lg-4 col-sm-6 col-12">
                            <div class="interview-video">
                                <iframe style="width:100%;height:220px;margin-bottom:1rem;" src="https://www.youtube.com/embed/_ZS12xpUS7A" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                                <p>Interview with Dumindra Ratnayake on Business Today</p>
                            </div>
                        </div>

                        <div class="col-lg-4 col-sm-6 col-12">
                            <div class="interview-video">
                                <iframe style="width:100%;height:220px;margin-bottom:1rem;" src="https://www.youtube.com/embed/ndKIhCEulJo" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                                <p>Interview with Dr. Harsha De Silva on Business Today</p>
                            </div>
                        </div>

                        <div class="col-lg-4 col-sm-6 col-12">
                            <div class="interview-video">
                                <iframe style="width:100%;height:220px;margin-bottom:1rem;" src="https://www.youtube.com/embed/B7I5ViQXOtI" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                                <p>Interview with M.G. Kularathne on Business Today</p>
                            </div>
                        </div>

                        <div class="col-lg-4 col-sm-6 col-12">
                            <div class="interview-video">
                                <iframe style="width:100%;height:220px;margin-bottom:1rem;" src="https://www.youtube.com/embed/xEbvsgBC3EI" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                                <p>Interview with Fazul Hameed on Business Today</p>
                            </div>
                        </div>

                        <div class="col-lg-4 col-sm-6 col-12">
                            <div class="interview-video">
                                <iframe style="width:100%;height:220px;margin-bottom:1rem;" src="https://www.youtube.com/embed/lPzFL7MSkwE" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                                <p>Interview with Dinesh Weerakody on Business Today</p>
                            </div>
                        </div>
                        
                            </div>
                        </div>
                        
                    </div>
                
                        <div class="col-12 mt-4">
                            <button id="show-more" type="button" class="btn btn-outline-dark w-100 show-more">SHOW ME MORE</button>
                        </div>
                     </div>
                </div> -->
                
         
                <div class="container">
                    <div class="tab-content">
                        
                        <div class="tab-pane active" id="tabs-1" role="tabpanel">
                            <div class="container">
                                <div class="row">
                                    <div class="col-lg-4 col-sm-6 col-12">
                                        <div class="interview-video">
                                            <iframe style="width:100%;height:220px;margin-bottom:1rem;" src="https://www.youtube.com/embed/WWLP9QypdQc" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                                            <p>Interview with Ranjith Pandithage</p>
                                        </div>
                                    </div>

                                    <div class="col-lg-4 col-sm-6 col-12">
                                        <div class="interview-video">
                                            <iframe style="width:100%;height:220px;margin-bottom:1rem;" src="https://www.youtube.com/embed/lYJUW5UGjYo" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                                            <p>Interview with Kishu Gomes on Buisness Today</p>
                                        </div>
                                    </div>

                                    <div class="col-lg-4 col-sm-6 col-12">
                                        <div class="interview-video">
                                            <iframe style="width:100%;height:220px;margin-bottom:1rem;" src="https://www.youtube.com/embed/3fP-K487tX0" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                                            <p>Interview with Ramani Fernando on Business Today</p>
                                        </div>
                                    </div>

                                    <div class="col-lg-4 col-sm-6 col-12">
                                        <div class="interview-video">
                                            <iframe style="width:100%;height:220px;margin-bottom:1rem;" src="https://www.youtube.com/embed/PJMKpqqd0fk" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                                            <p>Interview with Ajith Gunawardena on Business Today</p>
                                        </div>
                                    </div>


                                    <div class="col-lg-4 col-sm-6 col-12">
                                        <div class="interview-video">
                                            <iframe style="width:100%;height:220px;margin-bottom:1rem;" src="https://www.youtube.com/embed/gMb9uH2G2Zc" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                                            <p>Interview with Rohan Petiyagoda on Business Today</p>
                                        </div>
                                    </div>

                                    <div class="col-lg-4 col-sm-6 col-12">
                                        <div class="interview-video">
                                            <iframe style="width:100%;height:220px;margin-bottom:1rem;" src="https://www.youtube.com/embed/SE-mMLMkqxM" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                                            <p>Interview with Bramanage Premalal on Business Today</p>
                                        </div>
                                    </div>

                                    <div class="col-lg-4 col-sm-6 col-12">
                                        <div class="interview-video">
                                            <iframe style="width:100%;height:220px;margin-bottom:1rem;" src="https://www.youtube.com/embed/pCG2UXlO2v8" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                                            <p>Interview with Dr. Harsha Cabral on Business Today</p>
                                        </div>
                                    </div>

                                    <div class="col-lg-4 col-sm-6 col-12">
                                        <div class="interview-video">
                                            <iframe style="width:100%;height:220px;margin-bottom:1rem;" src="https://www.youtube.com/embed/FAzFVidCyVs" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                                            <p>Interview with Channa De Silva on Business Today</p>
                                        </div>
                                    </div>

                                    <div class="col-lg-4 col-sm-6 col-12">
                                        <div class="interview-video">
                                            <iframe style="width:100%;height:220px;margin-bottom:1rem;" src="https://www.youtube.com/embed/vCIwt3spJgE" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                                            <p>Interview with Dayan Gomes on Business Today</p>
                                        </div>
                                    </div>

                                
                                </div>
                            </div>
                        </div>
                        
                        <div class="tab-pane" id="tabs-2" role="tabpanel">
                            <div class="container">
                                <div class="row">

                                    <div class="col-lg-4 col-sm-6 col-12">
                                        <div class="interview-video">
                                            <iframe style="width:100%;height:220px;margin-bottom:1rem;" src="https://www.youtube.com/embed/wKvxOQRhKq8" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                                            <p>Interview with Eric Sooriyasena on Business Today</p>
                                        </div>
                                    </div>

                                    <div class="col-lg-4 col-sm-6 col-12">
                                        <div class="interview-video">
                                            <iframe style="width:100%;height:220px;margin-bottom:1rem;" src="https://www.youtube.com/embed/_ZS12xpUS7A" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                                            <p>Interview with Dumindra Ratnayake on Business Today</p>
                                        </div>
                                    </div>

                                    <div class="col-lg-4 col-sm-6 col-12">
                                        <div class="interview-video">
                                            <iframe style="width:100%;height:220px;margin-bottom:1rem;" src="https://www.youtube.com/embed/ndKIhCEulJo" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                                            <p>Interview with Dr. Harsha De Silva on Business Today</p>
                                        </div>
                                    </div>

                                    <div class="col-lg-4 col-sm-6 col-12">
                                        <div class="interview-video">
                                            <iframe style="width:100%;height:220px;margin-bottom:1rem;" src="https://www.youtube.com/embed/B7I5ViQXOtI" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                                            <p>Interview with M.G. Kularathne on Business Today</p>
                                        </div>
                                    </div>

                                    <div class="col-lg-4 col-sm-6 col-12">
                                        <div class="interview-video">
                                            <iframe style="width:100%;height:220px;margin-bottom:1rem;" src="https://www.youtube.com/embed/xEbvsgBC3EI" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                                            <p>Interview with Fazul Hameed on Business Today</p>
                                        </div>
                                    </div>

                                    <div class="col-lg-4 col-sm-6 col-12">
                                        <div class="interview-video">
                                            <iframe style="width:100%;height:220px;margin-bottom:1rem;" src="https://www.youtube.com/embed/lPzFL7MSkwE" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                                            <p>Interview with Dinesh Weerakody on Business Today</p>
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>
                        
                    </div>
                    <ul class="nav nav-tabs" role="tablist">
                        <li class="nav-item">
                            <a class="nav-link active" data-toggle="tab" href="#tabs-1" role="tab">1</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" data-toggle="tab" href="#tabs-2" role="tab">2</a>
                        </li>
                    </ul><!-- Tab panes -->
                    
                    
                
                </div>
   
    </main>
    <?php include 'includes/page-footer.php'; ?>
    <?php include 'includes/common-scripts.php'; ?>
 
     <?php include 'audioplyer.php'; ?>
</body>
</html>
    <style>
        #show-more-container {
            display: none;
            width: 100%;
        }
    </style>
    
<!--
    <script>
        
        $( document ).ready(function() {
            $("#show-more").click(function(){
              $("#show-more-container").slideDown();
            });
        });
    
    </script>
-->
