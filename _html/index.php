<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
    <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' name='viewport' />
    <title>Music</title>
    
    <?php include 'includes/common-doc-head.php'; ?>

</head>
<body>
    <?php include 'includes/page-header.php'; ?>

    <div class="container-fluid main-banner-slider">
        <div class="list-item-gallery ">
            <div class="gallery-hero owl-carousel">

                <div class="gallery-item">
                    <figure class="list-item-image">
                        <img src="assets/images/main-slide-image-1.jpg" alt="Music">
                    </figure>
                </div>

                <!-- <div class="gallery-item">
                    <figure class="list-item-image">
                    <img src="assets/images/main-slide-image-1.jpg" alt="Music">   
                    </figure>
                </div> -->
            </div>
        </div>
    </div>
    <main id="landing-page" class="main-content">

        <div class="main-content-inner">
            <div class="preloader"><span></span></div>
            <div class="page-content">
                <!-- <div class="container">
                    <div class="row">                        
                        <div class="col-lg-3 col-sm-6 col-12">
                            <div class="music-image">
                                <img src="assets/images/8.jpg" class="img-fluid" alt="img" >
                                <div class="player-container">
                                    <iframe src="https://open.spotify.com/embed/album/16rTsMjlDt6DEbLRtxvcWu" width="" height="" frameborder="0" allowtransparency="true" allow="encrypted-media"></iframe>
                                </div>
                            </div>
                        </div>

                        <div class="col-lg-3 col-sm-6 col-12">
                            <div class="music-image">
                                <img src="assets/images/8.jpg" class="img-fluid" alt="img" >
                                <div class="player-container">
                                    <iframe src="https://open.spotify.com/embed/album/1iSTXHBhLc9ImaqyvVZGft" width="" height="" frameborder="0" allowtransparency="true" allow="encrypted-media"></iframe>
                                </div>
                            </div>                            
                        </div>

                        <div class="col-lg-3 col-sm-6 col-12">
                            <div class="music-image">
                                <img src="assets/images/8.jpg" class="img-fluid" alt="img" >
                                <div class="player-container">
                                    <iframe src="https://open.spotify.com/embed/album/7aDBFWp72Pz4NZEtVBANi9" width="" height="" frameborder="0" allowtransparency="true" allow="encrypted-media"></iframe>
                                </div>
                            </div>
                        </div>

                        <div class="col-lg-3 col-sm-6 col-12">
                            <div class="music-image">
                                <img src="assets/images/8.jpg" class="img-fluid" alt="img" >
                                <div class="player-container">
                                    <iframe src="https://open.spotify.com/embed/album/1ibYM4abQtSVQFQWvDSo4J" width="" height="" frameborder="0" allowtransparency="true" allow="encrypted-media"></iframe>
                                </div>
                            </div>
                        </div>

                     </div>
                </div> -->

                <div class="container">
                    <div class="row">
                    <div class="col-lg-3 col-sm-6 col-12">
                        <div class="music-image">
                            <img src="assets/images/writer-image/writer-image-1.jpg" class="img-fluid" alt="img" >
                            <div class="player-container">
                                <div class="example">
                                    <ul class="playlist" style="display: none;">
                                        <li data-cover="assets/images/writer-image/writer-image-1.jpg" data-artist="Nisanga Mayadunne"><a
                                                href="assets/songs/aparimitha_wu.mp3">Aparimitha Wu</a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-lg-3 col-sm-6 col-12">
                        <div class="music-image">
                            <img src="assets/images/writer-image/writer-image-2.jpg" class="img-fluid" alt="img" >
                            <div class="player-container">
                                <div class="example">
                                    <ul class="playlist" style="display: none;">
                                        <li data-cover="assets/images/writer-image/writer-image-2.jpg" data-artist="Nisanga Mayadunne"><a
                                                href="assets/songs/karuna_meth_mudhitha.mp3">Karuna Meth Mudhitha</a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3 col-sm-6 col-12">
                        <div class="music-image">
                            <img src="assets/images/writer-image/writer-image-5.jpg" class="img-fluid" alt="img" >
                            <div class="player-container">
                                <div class="example">
                                    <ul class="playlist" style="display: none;">
                                        <li data-cover="assets/images/writer-image/writer-image-5.jpg" data-artist="Nisanga Mayadunne"><a
                                                href="assets/songs/manamaliya_wee.mp3">Manamaliya Wee</a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3 col-sm-6 col-12">
                        <div class="music-image">
                            <img src="assets/images/writer-image/writer-image-4.JPG" class="img-fluid" alt="img" >
                            <div class="player-container">
                                <div class="example">
                                    <ul class="playlist" style="display: none;">
                                        <li data-cover="assets/images/writer-image/writer-image-4.JPG" data-artist="Nisanga Mayadunne"><a
                                                href="assets/songs/nuba_lagin.mp3">Nuba Lagin</a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                        
                    <div class="col-lg-3 col-sm-6 col-12">
                        <div class="music-image">
                            <img src="assets/images/writer-image/writer-image-3.JPG" class="img-fluid" alt="img" >
                            <div class="player-container">
                                <div class="example">
                                    <ul class="playlist" style="display: none;">
                                        <li data-cover="assets/images/writer-image/writer-image-3.JPG" data-artist="Nisanga Mayadunne"><a
                                                href="assets/songs/pipi_mal_gomuwe.mp3">Pipi Mal Gomuwe</a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                        
                    <div class="col-lg-3 col-sm-6 col-12">
                        <div class="music-image">
                            <img src="assets/images/writer-image/writer-image-4.JPG" class="img-fluid" alt="img" >
                            <div class="player-container">
                                <div class="example">
                                    <ul class="playlist" style="display: none;">
                                        <li data-cover="assets/images/writer-image/writer-image-4.JPG" data-artist="Nisanga Mayadunne"><a
                                                href="assets/songs/piyanane.mp3">Piyanane</a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                        
                    <div class="col-lg-3 col-sm-6 col-12">
                        <div class="music-image">
                            <img src="assets/images/writer-image/writer-image-2.jpg" class="img-fluid" alt="img" >
                            <div class="player-container">
                                <div class="example">
                                    <ul class="playlist" style="display: none;">
                                        <li data-cover="assets/images/writer-image/writer-image-2.jpg" data-artist="Nisanga Mayadunne"><a
                                                href="assets/songs/sadak_lesin_paya.mp3">Sadak Lesin Paya</a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                        
                    <div class="col-lg-3 col-sm-6 col-12">
                        <div class="music-image">
                            <img src="assets/images/writer-image/writer-image-3.JPG" class="img-fluid" alt="img" >
                            <div class="player-container">
                                <div class="example">
                                    <ul class="playlist" style="display: none;">
                                        <li data-cover="assets/images/writer-image/writer-image-3.JPG" data-artist="Nisanga Mayadunne"><a
                                                href="assets/songs/sande_sisilath.mp3">Sade Siilath</a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
<!--
                    <div class="col-lg-3 col-sm-6 col-12">
                        <div class="music-image">
                            <img src="assets/images/writer-image/writer-image-5.jpg" class="img-fluid" alt="img" >
                            <div class="player-container">
                                <div class="example">
                                    <ul class="playlist" style="display: none;">
                                        <li data-cover="assets/images/writer-image/writer-image-5.jpg" data-artist="John Wesley"><a
                                                href="http://digital.akauk.com/utils/musicPlayer/data/Tequila 10 Seconds.mp3">Tequila 10 Seconds</a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
-->
<!--
                        <div class="col-lg-3 col-sm-6 col-12">
                            <div class="music-image">
                                <img src="assets/images/writer-image/writer-image-5.jpg" class="img-fluid" alt="img" >
                            </div>
                        </div>
-->
                        
<!--
                        <div class="col-lg-3 col-sm-6 col-12">
                            <div class="music-image">
                                <img src="assets/images/writer-image/writer-image-1.jpg" class="img-fluid" alt="img" >
                            </div>
                        </div>
-->
<!--
                        <div class="col-lg-3 col-sm-6 col-12">
                            <div class="music-image">
                                <img src="assets/images/writer-image/writer-image-2.jpg" class="img-fluid" alt="img" >
                            </div>
                        </div>
-->
<!--
                        <div class="col-lg-3 col-sm-6 col-12">
                            <div class="music-image">
                                <img src="assets/images/writer-image/writer-image-3.JPG" class="img-fluid" alt="img" >
                            </div>
                        </div>
-->
                     </div>
                </div>

                

                <div class="container">
                    <div class="music-backdrop">
                        <div class="row">
                            <div class="col-lg-8">
                                <div class="backdrop-content">
                                    <iframe src="https://www.youtube.com/embed/-YA8Uh8u0vo" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                                    <a class="backdrop-btn">View More Videos</a>
                                </div>
                            </div>
                            <div class="col-lg-4">
                                <div class="backdrop-content">
                                    <img src="assets/images/writer-image-yt.jpg" class="img-fluid" alt="img" >
                                    <a class="backdrop-btn">View More Videos</a>
                                    <div class="player-container" style="top:0;">
                                        <div class="example">
                                            <ul class="playlist" style="display: none;">
                                                <li data-cover="assets/images/writer-image-yt.jpg" data-artist="Nisanga Mayadunne"><a
                                                        href="assets/songs/sevanellak_vee.mp3">Sevanellak Vee</a></li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>                        
                     </div>
                </div>
    </main>
    <?php include 'includes/page-footer.php'; ?>
    <?php include 'includes/common-scripts.php'; ?>

    <script type="text/javascript">
        (function($) {
            "use strict";

            $('.gallery-hero').owlCarousel({
                margin: 15,
                nav: true,
                dots: true,
                items: 1,
                smartSpeed: 600,
                responsive:{
                    420: {
                        items: 1,
                    },
                    576: {
                        items: 1,
                    },
                    992: {
                        margin: 30,
                        items: 1,
                    }
                }
            });

        })(jQuery);
    </script>
    
<!--     <script type="text/javascript" src="assets/js/libs/jPlayer/jquery.jplayer.min.js"></script>
    <script type="text/javascript" src="assets/js/libs/jPlayer/playlist/jplayer.playlist.min.js"></script> -->
    

        <?php include 'audioplyer.php'; ?>

</body>
</html>
