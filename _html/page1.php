<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
    <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' name='viewport' />
    <title>Music</title>
    <?php include 'includes/common-doc-head.php'; ?>
    <style>
        .nav-tabs {
            border-bottom: none;
            width: 100px;
            margin: 0 auto;
            margin-top: 1rem;
        }

        .nav-tabs .nav-link {
            color: #000;
            background-color: #fff;
            border-radius: 0;
            border-color: #ccc;
        }
        .nav-link .active {
            color: red;
            background-color: blue;
            border-color: #000;
        }
        
        .nav-tabs .nav-link:hover {
            border-color: #ccc;
            background-color: #ccc;
            border-color: #ccc;
        }

        .nav-tabs .nav-link.active, .nav-tabs .nav-item.show .nav-link {
            color: #fff;
            background-color: #000;
            border-color: #dee2e6 #dee2e6 #fff;
        }
        
        .music-data p {
            margin-bottom: 0;
        }
        .watch-video {
            color: #505050;
            text-decoration: underline;
        }
        
        .page-link {
            position: absolute;
            display: inline-block;
            width: 100%;
            height: 100%;
            background-color: transparent;
            border: none;
            z-index: 2;
            -webkit-transition: all 0.2s ease;
            -moz-transition: all 0.2s ease;
            -ms-transition: all 0.2s ease;
            -o-transition: all 0.2s ease;
            transition: all 0.2s ease;
        }
        .page-link:hover {
            border: none;
            background-color: rgba(0,0,0,0.2);
/*            background-color: rgba(255,255,255,0.1);*/
        }
        
    </style>
</head>
<body>
    <?php include 'includes/page-header.php'; ?>

    <main id="landing-page" class="main-content pt-0">
        <div class="container position">
            <div class="list-item-gallery ">
                  <div class="top-main-banner w-75">
                      <img src="assets/images/filte1.jpg" alt="Music">
                  </div>
            </div>
            <div class="main-banner">
                <div class="gray-blure"></div>
            </div>
        </div>
        <div class="container">
            <nav class="navbar navbar-expand-lg navbar-light w-75 mx-auto p-0  mt-2">

              <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
              </button>

              <div class="collapse navbar-collapse row d-flex justify-content-end no-gutters" id="navbarSupportedContent">
                <ul class="navbar-nav col-12 col-sm-5">
                  <li class="nav-item active">
                    <a class="nav-link" href="#">All <span class="sr-only">(current)</span></a>
                  </li>
                  <li class="nav-item">
                    <a class="nav-link" href="#">Official</a>
                  </li>
                  <li class="nav-item">
                    <a class="nav-link" href="#">Interviews</a>
                  </li>
                  <li class="nav-item">
                    <a class="nav-link" href="#">Short Films</a>
                  </li>
                </ul>
                <form class="col-12 col-sm-5">
                    <div class="">
                        <div class="input-group">
                            
                            <input type="text" class="form-control" name="searchtext" placeholder="SEARCH MOVIES & TV">
                            <div class="input-group-append">
                                <button type="submit" class="btn btn-search">
                                    <i aria-hidden="true"><svg version="1.1" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="16px" height="16px" viewBox="0 0 16 16">
                                    <path d="M15.561,13.438l-3.672-3.67c-0.546,0.85-1.27,1.574-2.119,2.121l3.67,3.671c0.586,0.586,1.536,0.586,2.121,0 C16.146,14.975,16.146,14.025,15.561,13.438"></path>
                                    <path d="M11.999,6c0-3.313-2.686-6-5.999-6C2.686,0,0,2.687,0,6s2.686,5.999,6,5.999 C9.313,11.999,11.999,9.313,11.999,6 M6,10.499c-2.481,0-4.5-2.018-4.5-4.499S3.519,1.5,6,1.5s4.5,2.019,4.5,4.5 S8.481,10.499,6,10.499"></path></svg></i>
                                </button>
                            </div>
                        </div>
                    </div>
                </form>
              </div>
            </nav>
        </div>
        <br>
        <br>
        <div class="main-content-inner">
            <div class="preloader"><span></span></div>
            <div class="page-content">

                <!--<div class="container pt-4">
                    <div class="row">
                       
                        
                        <div class="col-lg-3 col-sm-6 col-12">
                        <div class="music-image">
                            <img src="assets/images/writer-image/writer-image-1.jpg" class="img-fluid" alt="img" >
                            <div class="player-container">
                                <div class="example">
                                    <ul class="playlist" style="display: none;">
                                        <li data-cover="assets/images/writer-image/writer-image-1.jpg" data-artist="Nisanga Mayadunne"><a
                                                href="assets/songs/aparimitha_wu.mp3">Aparimitha Wu</a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-lg-3 col-sm-6 col-12">
                        <div class="music-image">
                            <img src="assets/images/writer-image/writer-image-2.jpg" class="img-fluid" alt="img" >
                            <div class="player-container">
                                <div class="example">
                                    <ul class="playlist" style="display: none;">
                                        <li data-cover="assets/images/writer-image/writer-image-2.jpg" data-artist="Nisanga Mayadunne"><a
                                                href="assets/songs/karuna_meth_mudhitha.mp3">Karuna Meth Mudhitha</a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3 col-sm-6 col-12">
                        <div class="music-image">
                            <img src="assets/images/writer-image/writer-image-5.jpg" class="img-fluid" alt="img" >
                            <div class="player-container">
                                <div class="example">
                                    <ul class="playlist" style="display: none;">
                                        <li data-cover="assets/images/writer-image/writer-image-5.jpg" data-artist="Nisanga Mayadunne"><a
                                                href="assets/songs/manamaliya_wee.mp3">Manamaliya Wee</a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3 col-sm-6 col-12">
                        <div class="music-image">
                            <img src="assets/images/writer-image/writer-image-4.JPG" class="img-fluid" alt="img" >
                            <div class="player-container">
                                <div class="example">
                                    <ul class="playlist" style="display: none;">
                                        <li data-cover="assets/images/writer-image/writer-image-4.JPG" data-artist="Nisanga Mayadunne"><a
                                                href="assets/songs/nuba_lagin.mp3">Nuba Lagin</a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                        
                    <div class="col-lg-3 col-sm-6 col-12">
                        <div class="music-image">
                            <img src="assets/images/writer-image/writer-image-3.JPG" class="img-fluid" alt="img" >
                            <div class="player-container">
                                <div class="example">
                                    <ul class="playlist" style="display: none;">
                                        <li data-cover="assets/images/writer-image/writer-image-3.JPG" data-artist="Nisanga Mayadunne"><a
                                                href="assets/songs/pipi_mal_gomuwe.mp3">Pipi Mal Gomuwe</a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                        
                    <div class="col-lg-3 col-sm-6 col-12">
                        <div class="music-image">
                            <img src="assets/images/writer-image/writer-image-4.JPG" class="img-fluid" alt="img" >
                            <div class="player-container">
                                <div class="example">
                                    <ul class="playlist" style="display: none;">
                                        <li data-cover="assets/images/writer-image/writer-image-4.JPG" data-artist="Nisanga Mayadunne"><a
                                                href="assets/songs/piyanane.mp3">Piyanane</a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                        
                    <div class="col-lg-3 col-sm-6 col-12">
                        <div class="music-image">
                            <img src="assets/images/writer-image/writer-image-2.jpg" class="img-fluid" alt="img" >
                            <div class="player-container">
                                <div class="example">
                                    <ul class="playlist" style="display: none;">
                                        <li data-cover="assets/images/writer-image/writer-image-2.jpg" data-artist="Nisanga Mayadunne"><a
                                                href="assets/songs/sadak_lesin_paya.mp3">Sadak Lesin Paya</a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                        
                    <div class="col-lg-3 col-sm-6 col-12">
                        <div class="music-image">
                            <img src="assets/images/writer-image/writer-image-3.JPG" class="img-fluid" alt="img" >
                            <div class="player-container">
                                <div class="example">
                                    <ul class="playlist" style="display: none;">
                                        <li data-cover="assets/images/writer-image/writer-image-3.JPG" data-artist="Nisanga Mayadunne"><a
                                                href="assets/songs/sande_sisilath.mp3">Sade Siilath</a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                        
                        
                    
                    <div id="show-more-container">                        
                        
                        <div class="container pt-4">
                            <div class="row">
                        
                                <div class="col-lg-3 col-sm-6 col-12">
                                    <div class="music-image">
                                        <img src="assets/images/writer-image/writer-image-1.jpg" class="img-fluid" alt="img" >
                                        <div class="player-container">
                                            <div class="example">
                                                <ul class="playlist" style="display: none;">
                                                    <li data-cover="assets/images/writer-image/writer-image-1.jpg" data-artist="Nisanga Mayadunne"><a
                                                            href="assets/songs/dethol_wila_matha.mp3">Dethol Wila Matha</a></li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                        
                                <div class="col-lg-3 col-sm-6 col-12">
                                    <div class="music-image">
                                        <img src="assets/images/writer-image/writer-image-2.jpg" class="img-fluid" alt="img" >
                                        <div class="player-container">
                                            <div class="example">
                                                <ul class="playlist" style="display: none;">
                                                    <li data-cover="assets/images/writer-image/writer-image-2.jpg" data-artist="Nisanga Mayadunne"><a
                                                            href="assets/songs/dolos_mahe_sanda.mp3">Dolos Mahe Sanda</a></li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                        
                                <div class="col-lg-3 col-sm-6 col-12">
                                    <div class="music-image">
                                        <img src="assets/images/writer-image/writer-image-3.JPG" class="img-fluid" alt="img" >
                                        <div class="player-container">
                                            <div class="example">
                                                <ul class="playlist" style="display: none;">
                                                    <li data-cover="assets/images/writer-image/writer-image-3.JPG" data-artist="Nisanga Mayadunne"><a
                                                            href="assets/songs/langin_hitiyath.mp3">Lagin Hitiyath</a></li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                        
                                <div class="col-lg-3 col-sm-6 col-12">
                                    <div class="music-image">
                                        <img src="assets/images/writer-image/writer-image-4.JPG" class="img-fluid" alt="img" >
                                        <div class="player-container">
                                            <div class="example">
                                                <ul class="playlist" style="display: none;">
                                                    <li data-cover="assets/images/writer-image/writer-image-4.JPG" data-artist="Nisanga Mayadunne"><a
                                                            href="assets/songs/mal_hasarel.mp3">Mal Hasarel</a></li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </div> 

                                <div class="col-lg-3 col-sm-6 col-12">
                                    <div class="music-image">
                                        <img src="assets/images/writer-image/writer-image-5.jpg" class="img-fluid" alt="img" >
                                        <div class="player-container">
                                            <div class="example">
                                                <ul class="playlist" style="display: none;">
                                                    <li data-cover="assets/images/writer-image/writer-image-5.jpg" data-artist="Nisanga Mayadunne"><a
                                                            href="assets/songs/nuba_awidin.mp3">Nuba Awidin</a></li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                        
                        
                                <div class="col-lg-3 col-sm-6 col-12">
                                    <div class="music-image">
                                        <img src="assets/images/writer-image/writer-image-1.jpg" class="img-fluid" alt="img" >
                                        <div class="player-container">
                                            <div class="example">
                                                <ul class="playlist" style="display: none;">
                                                    <li data-cover="assets/images/writer-image/writer-image-1.jpg" data-artist="Nisanga Mayadunne"><a
                                                            href="assets/songs/nuba_kohedai.MP3">Nuba Kohedai</a></li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                 <div class="col-lg-3 col-sm-6 col-12">
                                    <div class="music-image">
                                        <img src="assets/images/writer-image/writer-image-2.jpg" class="img-fluid" alt="img" >
                                        <div class="player-container">
                                            <div class="example">
                                                <ul class="playlist" style="display: none;">
                                                    <li data-cover="assets/images/writer-image/writer-image-2.jpg" data-artist="Nisanga Mayadunne"><a
                                                            href="assets/songs/oba_paewa_sina.mp3">Oba Peewa Sina</a></li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                        
                                <div class="col-lg-3 col-sm-6 col-12">
                                    <div class="music-image">
                                        <img src="assets/images/writer-image/writer-image-4.JPG" class="img-fluid" alt="img" >
                                        <div class="player-container">
                                            <div class="example">
                                                <ul class="playlist" style="display: none;">
                                                    <li data-cover="assets/images/writer-image/writer-image-4.JPG" data-artist="Nisanga Mayadunne"><a
                                                            href="assets/songs/pinna_wetenaa.mp3">Pinna Wetenaa</a></li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                    <div class="col-lg-3 col-sm-6 col-12">
                                    <div class="music-image">
                                        <img src="assets/images/writer-image/writer-image-3.JPG" class="img-fluid" alt="img" >
                                        <div class="player-container">
                                            <div class="example">
                                                <ul class="playlist" style="display: none;">
                                                    <li data-cover="assets/images/writer-image/writer-image-3.JPG" data-artist="Nisanga Mayadunne"><a
                                                            href="assets/songs/sansaraye.mp3">Sansaraye</a></li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                        
                                <div class="col-lg-3 col-sm-6 col-12">
                                    <div class="music-image">
                                        <img src="assets/images/writer-image/writer-image-5.jpg" class="img-fluid" alt="img" >
                                        <div class="player-container">
                                            <div class="example">
                                                <ul class="playlist" style="display: none;">
                                                    <li data-cover="assets/images/writer-image/writer-image-5.jpg" data-artist="Nisanga Mayadunne"><a
                                                            href="assets/songs/sithe_siraunu.mp3">Sithe Siraunu</a></li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                        
                                <div class="col-lg-3 col-sm-6 col-12">
                                    <div class="music-image">
                                        <img src="assets/images/writer-image/writer-image-1.jpg" class="img-fluid" alt="img" >
                                        <div class="player-container">
                                            <div class="example">
                                                <ul class="playlist" style="display: none;">
                                                    <li data-cover="assets/images/writer-image/writer-image-1.jpg" data-artist="Nisanga Mayadunne"><a
                                                            href="assets/songs/yanna_denna_mata.mp3">Yanna Denna Mata</a></li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        
                    </div>
                
                        <div class="col-12 mt-4">
                            <button id="show-more" type="button" class="btn btn-outline-dark w-100 show-more">SHOW ME MORE</button>
                        </div>
                     </div>
                </div> -->
                
                <div class="container">
                    <div class="tab-content">
                        
                        <div class="tab-pane active" id="tabs-1" role="tabpanel">
                            <div class="container">
                            <div class="row">
                                
                                <div class="col-lg-3 col-sm-6 col-12">
                                    <div class="music-wrapper">
                                        <div class="music-image">
                                            <img src="assets/images/writer-image/writer-image-2.jpg" class="img-fluid" alt="img">
                                            <a class="page-link" href="karuna-meth-mudhitha.php"></a>
                                            <div class="player-container">
                                                <div class="example">
                                                    <ul class="playlist" style="display: none;">
                                                        <li data-cover="assets/images/writer-image/writer-image-2.jpg" data-artist="Nisanga Mayadunne"><a
                                                                href="assets/songs/karuna_meth_mudhitha.mp3">Karuna Meth Mudhitha</a></li>
                                                    </ul>
                                                </div>
                                            </div>                                        
                                        </div>
                                    </div>
                                </div>
                                
                                 <div class="col-lg-3 col-sm-6 col-12">
                                    <div class="music-wrapper">
                                        <div class="music-image">
                                            <img src="assets/images/writer-image/writer-image-1.jpg" class="img-fluid" alt="img" >
                                            <a class="page-link" href="aparimitha-wu.php"></a>
                                            <div class="player-container">
                                                <div class="example">
                                                    <ul class="playlist" style="display: none;">
                                                        <li data-cover="assets/images/writer-image/writer-image-1.jpg" data-artist="Nisanga Mayadunne"><a
                                                                href="assets/songs/aparimitha_wu.mp3">Aparimitha Wu</a></li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                
                                <div class="col-lg-3 col-sm-6 col-12">
                                    <div class="music-wrapper">
                                        <div class="music-image">
                                            <img src="assets/images/writer-image/writer-image-4.JPG" class="img-fluid" alt="img" >
                                            <a class="page-link" href="piyanane.php"></a>
                                            <div class="player-container">
                                                <div class="example">
                                                    <ul class="playlist" style="display: none;">
                                                        <li data-cover="assets/images/writer-image/writer-image-4.JPG" data-artist="Nisanga Mayadunne"><a
                                                                href="assets/songs/piyanane.mp3">Piyanane</a></li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                
                                <div class="col-lg-3 col-sm-6 col-12">
                                    <div class="music-wrapper">
                                        <div class="music-image">
                                            <img src="assets/images/writer-image/writer-image-2.jpg" class="img-fluid" alt="img" >
                                            <a class="page-link" href="dolos-mahe-sanda.php"></a>
                                            <div class="player-container">
                                                <div class="example">
                                                    <ul class="playlist" style="display: none;">
                                                        <li data-cover="assets/images/writer-image/writer-image-2.jpg" data-artist="Nisanga Mayadunne"><a
                                                                href="assets/songs/dolos_mahe_sanda.mp3">Dolos Mahe Sanda</a></li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                
                                <div class="col-lg-3 col-sm-6 col-12">
                                    <div class="music-wrapper">
                                        <div class="music-image">
                                            <img src="assets/images/writer-image/writer-image-2.jpg" class="img-fluid" alt="img" >
                                            <a class="page-link" href="manaliya-wee.php"></a>
                                            <div class="player-container">
                                                <div class="example">
                                                    <ul class="playlist" style="display: none;">
                                                        <li data-cover="assets/images/writer-image/writer-image-2.jpg" data-artist="Nisanga Mayadunne"><a
                                                                href="assets/songs/manamaliya_wee.mp3">Manamaliya Wee</a></li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                
                                <div class="col-lg-3 col-sm-6 col-12">
                                    <div class="music-wrapper">
                                        <div class="music-image">
                                            <img src="assets/images/writer-image/writer-image-3.JPG" class="img-fluid" alt="img" >
                                            <a class="page-link" href="sevanellak-wee.php"></a>
                                            <div class="player-container">
                                                <div class="example">
                                                    <ul class="playlist" style="display: none;">
                                                        <li data-cover="assets/images/writer-image/writer-image-3.JPG" data-artist="Nisanga Mayadunne"><a
                                                                href="assets/songs/sevanellak_vee.mp3">Sevanellak Wee</a></li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                
                                <div class="col-lg-3 col-sm-6 col-12">
                                    <div class="music-wrapper">
                                        <div class="music-image">
                                            <img src="assets/images/writer-image/writer-image-4.JPG" class="img-fluid" alt="img" >
                                            <a class="page-link" href="sade-sisilath.php"></a>
                                            <div class="player-container">
                                                <div class="example">
                                                    <ul class="playlist" style="display: none;">
                                                        <li data-cover="assets/images/writer-image/writer-image-4.JPG" data-artist="Nisanga Mayadunne"><a
                                                                href="assets/songs/sande_sisilath.mp3">Sade Siilath</a></li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                
                                <div class="col-lg-3 col-sm-6 col-12">
                                    <div class="music-wrapper">
                                        <div class="music-image">
                                            <img src="assets/images/writer-image/writer-image-5.jpg" class="img-fluid" alt="img" >
                                            <a class="page-link" href="pipi-mal-gomuwe.php"></a>
                                            <div class="player-container">
                                                <div class="example">
                                                    <ul class="playlist" style="display: none;">
                                                        <li data-cover="assets/images/writer-image/writer-image-5.jpg" data-artist="Nisanga Mayadunne"><a
                                                                href="assets/songs/pipi_mal_gomuwe.mp3">Pipi Mal Gomuwe</a></li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                        
                                <div class="col-lg-3 col-sm-6 col-12">
                                    <div class="music-wrapper">
                                        <div class="music-image">
                                            <img src="assets/images/writer-image/writer-image-4.JPG" class="img-fluid" alt="img" >
                                            <a class="page-link" href="nuba-lagin.php"></a>
                                            <div class="player-container">
                                                <div class="example">
                                                    <ul class="playlist" style="display: none;">
                                                        <li data-cover="assets/images/writer-image/writer-image-4.JPG" data-artist="Nisanga Mayadunne"><a
                                                                href="assets/songs/nuba_lagin.mp3">Nuba Lagin</a></li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                
                                <div class="col-lg-3 col-sm-6 col-12">
                                    <div class="music-wrapper">
                                        <div class="music-image">
                                            <img src="assets/images/writer-image/writer-image-5.jpg" class="img-fluid" alt="img" >
                                            <a class="page-link" href="nuba-awidin.php"></a>
                                            <div class="player-container">
                                                <div class="example">
                                                    <ul class="playlist" style="display: none;">
                                                        <li data-cover="assets/images/writer-image/writer-image-5.jpg" data-artist="Nisanga Mayadunne"><a
                                                                href="assets/songs/nuba_awidin.mp3">Nuba Awidin</a></li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                
                                <div class="col-lg-3 col-sm-6 col-12">
                                    <div class="music-wrapper">
                                        <div class="music-image">
                                            <img src="assets/images/cover-image/sith-ahasa-cover-img.jpg" class="img-fluid" alt="img" >
                                            <a class="page-link" href="sith-ahasa.php"></a>
                                            <div class="player-container">
                                                <div class="example">
                                                    <ul class="playlist" style="display: none;">
                                                        <li data-cover="assets/images/writer-image/writer-image-1.jpg" data-artist="Nisanga Mayadunne"><a
                                                                href="assets/songs/sith_ahasa.mp3">Sith Ahasa</a></li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                
                                <div class="col-lg-3 col-sm-6 col-12">
                                    <div class="music-wrapper">
                                        <div class="music-image">
                                            <img src="assets/images/writer-image/writer-image-1.jpg" class="img-fluid" alt="img" >
                                            <a class="page-link" href="yanna-denna-mata.php"></a>
                                            <div class="player-container">
                                                <div class="example">
                                                    <ul class="playlist" style="display: none;">
                                                        <li data-cover="assets/images/writer-image/writer-image-1.jpg" data-artist="Nisanga Mayadunne"><a
                                                                href="assets/songs/yanna_denna_mata.mp3">Yanna Denna Mata</a></li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                
                            </div>
                            </div>
                        </div>
                        
                        <div class="tab-pane" id="tabs-2" role="tabpanel">
                            <div class="container">
                            <div class="row">
                                
                                <div class="col-lg-3 col-sm-6 col-12">
                                    <div class="music-wrapper">
                                        <div class="music-image">
                                            <img src="assets/images/writer-image/writer-image-4.JPG" class="img-fluid" alt="img" >
                                            <a class="page-link" href="pinna-wetena.php"></a>
                                            <div class="player-container">
                                                <div class="example">
                                                    <ul class="playlist" style="display: none;">
                                                        <li data-cover="assets/images/writer-image/writer-image-4.JPG" data-artist="Nisanga Mayadunne"><a
                                                                href="assets/songs/pinna_wetenaa.mp3">Pinna Wetenaa</a></li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                
                                <div class="col-lg-3 col-sm-6 col-12">
                                    <div class="music-wrapper">
                                        <div class="music-image">
                                            <img src="assets/images/writer-image/writer-image-4.JPG" class="img-fluid" alt="img" >
                                            <a class="page-link" href="mal-hasarel.php"></a>
                                            <div class="player-container">
                                                <div class="example">
                                                    <ul class="playlist" style="display: none;">
                                                        <li data-cover="assets/images/writer-image/writer-image-4.JPG" data-artist="Nisanga Mayadunne"><a
                                                                href="assets/songs/mal_hasarel.mp3">Mal Hasarel</a></li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                
                                <div class="col-lg-3 col-sm-6 col-12">
                                    <div class="music-wrapper">
                                        <div class="music-image">
                                            <img src="assets/images/writer-image/writer-image-2.jpg" class="img-fluid" alt="img" >
                                            <a class="page-link" href="oba-peewa-sina.php"></a>
                                            <div class="player-container">
                                                <div class="example">
                                                    <ul class="playlist" style="display: none;">
                                                        <li data-cover="assets/images/writer-image/writer-image-2.jpg" data-artist="Nisanga Mayadunne"><a
                                                                href="assets/songs/oba_paewa_sina.mp3">Oba Peewa Sina</a></li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                
                                
                                <div class="col-lg-3 col-sm-6 col-12">
                                    <div class="music-wrapper">
                                        <div class="music-image">
                                            <img src="assets/images/writer-image/writer-image-1.jpg" class="img-fluid" alt="img" >
                                            <a class="page-link" href="dethol-wila-matha.php"></a>
                                            <div class="player-container">
                                                <div class="example">
                                                    <ul class="playlist" style="display: none;">
                                                        <li data-cover="assets/images/writer-image/writer-image-1.jpg" data-artist="Nisanga Mayadunne"><a
                                                                href="assets/songs/dethol_wila_matha.mp3">Dethol Wila Matha</a></li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                
                                <div class="col-lg-3 col-sm-6 col-12">
                                    <div class="music-wrapper">
                                        <div class="music-image">
                                            <img src="assets/images/writer-image/writer-image-5.jpg" class="img-fluid" alt="img" >
                                            <a class="page-link" href="sithe-siraunu.php"></a>
                                            <div class="player-container">
                                                <div class="example">
                                                    <ul class="playlist" style="display: none;">
                                                        <li data-cover="assets/images/writer-image/writer-image-5.jpg" data-artist="Nisanga Mayadunne"><a
                                                                href="assets/songs/sithe_siraunu.mp3">Sithe Siraunu</a></li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                
                                <div class="col-lg-3 col-sm-6 col-12">
                                    <div class="music-wrapper">
                                        <div class="music-image">
                                            <img src="assets/images/writer-image/writer-image-1.jpg" class="img-fluid" alt="img" >
                                            <a class="page-link" href="nuba-kohedai.php"></a>
                                            <div class="player-container">
                                                <div class="example">
                                                    <ul class="playlist" style="display: none;">
                                                        <li data-cover="assets/images/writer-image/writer-image-1.jpg" data-artist="Nisanga Mayadunne"><a
                                                                href="assets/songs/nuba_kohedai.MP3">Nuba Kohedai</a></li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                
                                <div class="col-lg-3 col-sm-6 col-12">
                                    <div class="music-wrapper">
                                        <div class="music-image">
                                            <img src="assets/images/writer-image/writer-image-3.JPG" class="img-fluid" alt="img" >
                                            <a class="page-link" href="lagin-hitiyath.php"></a>
                                            <div class="player-container">
                                                <div class="example">
                                                    <ul class="playlist" style="display: none;">
                                                        <li data-cover="assets/images/writer-image/writer-image-3.JPG" data-artist="Nisanga Mayadunne"><a
                                                                href="assets/songs/langin_hitiyath.mp3">Lagin Hitiyath</a></li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                
                                <div class="col-lg-3 col-sm-6 col-12">
                                    <div class="music-wrapper">
                                        <div class="music-image">
                                            <img src="assets/images/writer-image/writer-image-2.jpg" class="img-fluid" alt="img" >
                                            <a class="page-link" href="sadak_lesin_paya.php"></a>
                                            <div class="player-container">
                                                <div class="example">
                                                    <ul class="playlist" style="display: none;">
                                                        <li data-cover="assets/images/writer-image/writer-image-2.jpg" data-artist="Nisanga Mayadunne"><a
                                                                href="assets/songs/sadak_lesin_paya.mp3">Sadak Lesin Paya</a></li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                        
                                
                                <div class="col-lg-3 col-sm-6 col-12">
                                    <div class="music-wrapper">
                                        <div class="music-image">
                                            <img src="assets/images/writer-image/writer-image-3.JPG" class="img-fluid" alt="img" >
                                            <a class="page-link" href="sansaraye.php"></a>
                                            <div class="player-container">
                                                <div class="example">
                                                    <ul class="playlist" style="display: none;">
                                                        <li data-cover="assets/images/writer-image/writer-image-3.JPG" data-artist="Nisanga Mayadunne"><a
                                                                href="assets/songs/sansaraye.mp3">Sansaraye</a></li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                        
                                
                        
                                
                            </div>
                        </div>
                        </div>
                        
                    </div>
                    <ul class="nav nav-tabs" role="tablist">
                        <li class="nav-item">
                            <a class="nav-link active" data-toggle="tab" href="#tabs-1" role="tab">1</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" data-toggle="tab" href="#tabs-2" role="tab">2</a>
                        </li>
                    </ul><!-- Tab panes -->
                    
                    
                
                </div>
   
    </main>
    <?php include 'includes/page-footer.php'; ?>
    <?php include 'includes/common-scripts.php'; ?>
 
     <?php include 'audioplyer.php'; ?>
</body>
</html>
    <style>
        #show-more-container {
            display: none;
            width: 100%;
        }
    </style>
    
<!--
    <script>
        
        $( document ).ready(function() {
            $("#show-more").click(function(){
              $("#show-more-container").slideDown();
            });
        });
    
    </script>
-->
