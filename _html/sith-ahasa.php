<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
    <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' name='viewport' />
    <title>Sith Ahasa</title>
    <?php include 'includes/common-doc-head.php'; ?>
    <style>
        .player-details {
            padding-bottom: 1rem;
        }
    </style>
</head>
<body>
    <?php include 'includes/page-header.php'; ?>

    <main id="landing-page" class="main-content pt-0">
        <div class="container">
            <div class="border-bottom pt-3 pb-3">
                <h1 class="mt-3">Sith Ahasa Pirivara</h1>
                <div class="row d-flex align-items-center">
                    <div class="col-sm-5">
                        <img class="img-fluid" src="assets/images/cover-image/sith-ahasa-cover-img.jpg">
                    </div>
                    <div class="col-sm-4">
                        <div class="mb-1 mt-4">
                            <div class="player-details">
                                <p>Songwriter : <span>Nisanga Mayadunne</span></p>
                                <p>Artist : <span>Shashika Nisansala</span></p>
                                <p>Music : <span>Sarath De Alwis</span></p>
                            </div>
                            <div class="player-container-single-page">
                                <div class="example">
                                    <ul class="playlist" style="display: none;">
                                        <li data-cover="assets/images/writer-image/writer-image-2.jpg" data-artist="Nisanga Mayadunne"><a
                                                href="assets/songs/sith_ahasa.mp3">Sith Ahasa Pirivara</a></li>
                                    </ul>
                                </div>
                            </div>
                            
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="container">
            <div class="border-bottom pt-4 pb-4">
                <h1>LYRICS</h1>
                <div class="row content">
                    <div class="col-5">
                        <p class="mb-0">Why should I feel discouraged,</p>
                        <p class="mb-0">Why should the shadows come,</p>
                        <p class="mb-0">Why should my heart be lonely,</p>
                        <p class="mb-0">And long for Heaven, Heaven and home,</p>
                        <p class="mb-0">When, When jesus in my portion,</p>
                        <p class="mb-0">My constant friend in He;</p>
                        <p class="mb-0">Oh, oh-oh, His eye is on the sperrow,</p>
                        <p class="mb-0">And I know He watches, watches over me</p>
                    </div>
                    <div class="col-5">
                        <p class="mb-0">Why should I feel discouraged,</p>
                        <p class="mb-0">Why should the shadows come,</p>
                        <p class="mb-0">Why should my heart be lonely,</p>
                        <p class="mb-0">And long for Heaven, Heaven and home,</p>
                    </div>
                </div>
            </div>
        </div>
        <div class="container">
            <div class="border-bottom pt-3 pb-3">
                <h1>VIDEOS</h1>
                <div class="row">

                    <div class="col-md-6">
                            <iframe style="width:100%;height:300px;margin-bottom:1rem;" src="https://www.youtube.com/embed/-YA8Uh8u0vo" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                    </div>

                    
                </div>
            </div>
        </div>
        <div class="container">
            <div class="border-bottom pt-3 pb-3">
                <div class="row">
                    <div class="col-12">
                        <a class="btn hide-comment p-0" data-toggle="collapse" href="#collapseExample" role="button" aria-expanded="false" aria-controls="collapseExample">
                            HIDE COMMENTS
                        </a>

                    <div class="collapse show" id="collapseExample">
                        <div class="row">
                            <div class="fb-feeds col-sm-6">
                                <div id="fb-root"></div>
                                    <script async defer crossorigin="anonymous" src="https://connect.facebook.net/en_US/sdk.js#xfbml=1&version=v6.0"></script>
                                <div class="fb-comments" data-href="https://www.facebook.com/richardmarxmusic/" data-width="60%" data-numposts="5"></div>
                            </div>
                        </div>
                    </div> 
                    </div>
                </div>
            </div>
        </div>


    </main>
    <?php include 'includes/page-footer.php'; ?>
    <?php include 'includes/common-scripts.php'; ?>
    <?php include 'audioplyer.php'; ?>
 
<script type="text/javascript">
    $('.hide-comment').click(function(){
        var $this = $(this);
        $this.toggleClass('hide-comment');
        if($this.hasClass('hide-comment')){
            $this.text('HIDE COMMENTS');         
        } else {
            
            $this.text('SHOW COMMENTS');
        }
    });
</script>
</body>
</html>
