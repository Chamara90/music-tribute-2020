(() => {

    'use strict';

    // Set envoironment mode
    //var env = process.env.NODE_ENV || 'development';
    const env = process.env.NODE_ENV = 'production';

    // Defining base pathes
    const paths = {
        bower: 'bower_components/',
        dev: 'src/',
        theme: '../_html/assets/',
        //theme: '../public/assets/',
    };

    // browser-sync watched files
    // automatically reloads the page when files changed
    const browserSyncWatchFiles = [
        paths.theme + 'css/*.min.css',
        paths.theme + 'js/*.min.js',
        paths.theme + 'images/**/*.*',
        './**/*.php'
    ];

    // browser-sync options
    // see: https://www.browsersync.io/docs/options/
    const browserSyncOptions = {
        proxy: 'localhost/music-tribute-2020/_html',
        open: true,
        notify: false
    };

    // Defining requirements
    const
        gulp = require('gulp'),
        gutil = require('gulp-util'),
        autoprefixer = require('gulp-autoprefixer'),
        jshint = require('gulp-jshint'),
        stylish = require('jshint-stylish'),
        plumber = require('gulp-plumber'),
        sass = require('gulp-sass'),
        watch = require('gulp-watch'),
        cssnano = require('gulp-cssnano'),
        rename = require('gulp-rename'),
        concat = require('gulp-concat'),
        uglify = require('gulp-uglify'),
        ignore = require('gulp-ignore'),
        rimraf = require('gulp-rimraf'),
        imagemin = require('gulp-imagemin'),
        pngcrush = require('imagemin-pngcrush'),
        gulpif = require('gulp-if'),
        sourcemaps = require('gulp-sourcemaps'),
        newer = require('gulp-newer'),
        minifycss = require('gulp-uglifycss'),
        filter = require('gulp-filter'),
        //cmq = require('gulp-combine-media-queries'),
        notify = require('gulp-notify'),
        browserSync = require('browser-sync').create(),
        reload = browserSync.reload;


    // Run:
    // gulp styles
    // Compile Sass, Autoprefix and minify
    function styles() {
        return  gulp.src(paths.dev + 'scss/**/*.scss')
        .pipe(plumber(function(error) {
            gutil.log(gutil.colors.red(error.message));
            this.emit('end');
        }))
        .pipe(sourcemaps.init())
        .pipe(sass({
            errLogToConsole: true,
            // outputStyle: 'compressed',
            // outputStyle: 'compact',
            // outputStyle: 'nested',
            outputStyle: 'expanded',
            precision: 10,
            includePaths: [
                paths.bower,
            ]
        }))
        .pipe(autoprefixer('last 2 version', '> 1%', 'safari 5', 'ie 8', 'ie 9', 'opera 12.1', 'ios 6', 'android 4'))
        .pipe(plumber.stop())
        .pipe(sourcemaps.write('.', {includeContent: false}))
        .pipe(gulp.dest(paths.theme + 'css'))
        .pipe(reload({stream:true})) // Inject Styles when min style file is created
        .pipe(notify({ message: 'Styles task complete', onLast: true }))
    };

    exports.styles = styles;


    function stylesMin() {
        return  gulp.src(paths.dev + 'scss/**/*.scss')
        .pipe(plumber(function(error) {
            gutil.log(gutil.colors.red(error.message));
            this.emit('end');
        }))
        .pipe(sass({
            errLogToConsole: true,
            outputStyle: 'expanded',
            precision: 10,
            includePaths: [
                paths.bower,
            ]
        }))
        .pipe(autoprefixer('last 2 version', '> 1%', 'safari 5', 'ie 8', 'ie 9', 'opera 12.1', 'ios 6', 'android 4'))
        .pipe(plumber.stop())
        .pipe(rename({ suffix: '.min' }))
        .pipe(minifycss({
            maxLineLen: 80,
            uglyComments: true
        }))
        .pipe(gulp.dest(paths.theme + 'css'))
        .pipe(reload({stream:true})) // Inject Styles when min style file is created
        .pipe(notify({ message: 'Styles task complete', onLast: true }))
    };

    exports.styles = stylesMin;



    // Run:
    // gulp scripts
    // JSHint, concat, and minify JavaScript
    function scripts() {
        return gulp.src([

            paths.bower + 'jquery-validation/dist/jquery.validate.js',
            // paths.bower + 'jquery-validation/dist/additional-methods.js',
            // paths.bower + 'EasyAutocomplete/dist/jquery.easy-autocomplete.js',
            // paths.bower + 'intl-tel-input/build/js/utils.js',
            // paths.bower + 'intl-tel-input/build/js/intlTelInput.js',
            // paths.bower + 'jquery-mask-plugin/dist/jquery.mask.js',
            // paths.bower + 'slick-carousel/slick/slick.js',
            paths.bower + 'owl.carousel/dist/owl.carousel.js',
            // paths.bower + 'fancybox/dist/jquery.fancybox.js',
            // paths.bower + 'aos/dist/aos.js',
            paths.bower + 'bootstrap-pincode-input/js/bootstrap-pincode-input.js',

            // paths.bower + 'jquery.steps/build/jquery.steps.js',
            // paths.bower + 'datatables.net/js/jquery.dataTables.js',
            // paths.bower + 'datatables.net-bs4/js/dataTables.bootstrap4.js',
            // paths.bower + 'numeral/numeral.js',
            paths.bower + 'exif-js/exif.js',
            // paths.bower + 'Croppie/croppie.js',
            // paths.bower + 'moment/moment.js',
            //paths.bower + 'tempusdominus-bootstrap-4/build/js/tempusdominus-bootstrap-4.js',
            // paths.bower + 'eonasdan-bootstrap-datetimepicker/build/js/bootstrap-datetimepicker.min.js',
            paths.bower + 'bootstrap-select/dist/js/bootstrap-select.js',
            // paths.bower + 'bootbox.js/dist/bootbox.min.js',
            paths.bower + 'lazysizes/lazysizes.min.js',
            // paths.bower + 'chart.js/dist/Chart.js',

            // Grab your custom scripts
            paths.dev + 'scripts/*.js',

        ])
        .pipe(plumber())
        .pipe(sourcemaps.init())
        .pipe(jshint())
        .pipe(jshint.reporter('jshint-stylish'))
        .pipe(concat('scripts.js'))
        .pipe(gulp.dest(paths.theme + 'js'))
        .pipe(rename({suffix: '.min'}))
        .pipe(uglify())
        .pipe(sourcemaps.write('.')) // Creates sourcemap for minified JS
        .pipe(gulp.dest(paths.theme + 'js'))
        .pipe(reload({stream: true}));
    };

    exports.scripts = scripts;

    // Run:
    // gulp customScripts
    // JSHint, concat, and minify Custom JavaScript
    function customScripts() {
        return gulp.src([

              paths.dev + 'scripts/custom/*.js',

        ])
        .pipe(plumber())
        .pipe(sourcemaps.init())
        .pipe(jshint())
        .pipe(jshint.reporter('jshint-stylish'))
        .pipe(gulp.dest(paths.theme + 'js'))
        .pipe(rename({suffix: '.min'}))
        .pipe(uglify())
        .pipe(sourcemaps.write('.')) // Creates sourcemap for minified JS
        .pipe(gulp.dest(paths.theme + 'js'))
        .pipe(reload({stream: true}));
    };

    exports.customScripts = customScripts;

    // Run:
    // gulp pageScripts
    // JSHint, concat, and minify Custom JavaScript
    function pageScripts() {
        return gulp.src([

            paths.dev + 'scripts/pages/*.js',

        ])
        .pipe(plumber())
        .pipe(sourcemaps.init())
        .pipe(jshint())
        .pipe(jshint.reporter('jshint-stylish'))
        .pipe(gulp.dest(paths.theme + 'js/pages'))
        .pipe(rename({suffix: '.min'}))
        .pipe(uglify())
        .pipe(sourcemaps.write('.')) // Creates sourcemap for minified JS
        .pipe(gulp.dest(paths.theme + 'js/pages'))
        .pipe(reload({stream: true}));
    };

    exports.pageScripts = pageScripts;


    // Run:
    // gulp vendorScripts
    // JSHint, concat, and minify Custom JavaScript
    function vendorScripts() {
        return gulp.src([

              paths.dev + 'scripts/vendor/**/*.js',

        ])
        .pipe(gulp.dest(paths.theme + 'js/vendor'))
        .pipe(reload({stream: true}));
    };

    exports.vendorScripts = vendorScripts;

    // Run:
    // gulp json
    // JSHint, concat, and minify Custom JavaScript
    function jsonScripts() {
        return gulp.src([

              paths.dev + 'scripts/json/*.json',

        ])
        .pipe(gulp.dest(paths.theme + 'js/json'))
        .pipe(reload({stream: true}));
    };

    exports.jsonScripts = jsonScripts;


    // Run:
    // gulp copyAssets
    // Copy all needed dependency assets files from bower_component assets to themes /js, /scss and /fonts folder. Run this task after bower install or bower update
    function copyAssets(done) {

        // Copy all Font Awesome Fonts
        gulp.src(paths.bower + 'font-awesome/fonts/**/*.{otf,ttf,woff,woff2,eot,svg}')
        .pipe(gulp.dest(paths.theme + 'fonts'));

        // Copy jQuery
        gulp.src(paths.bower + 'jquery/dist/*.**')
        .pipe(gulp.dest(paths.theme + 'js/libs/jquery'));

        // Copy Bootstrap Js Files
        gulp.src(paths.bower + 'bootstrap/dist/js/*.**')
        .pipe(gulp.dest(paths.theme + 'js/libs/bootstrap'));

        // Copy Popper JS
        gulp.src(paths.bower + 'popper.js/dist/umd/*.js')
        .pipe(gulp.dest(paths.theme + 'js/libs/popper'));

        // Copy Jquery UI
        // gulp.src(paths.bower + 'jquery-ui/**/**/*.**')
        // .pipe(gulp.dest(paths.theme + 'js/libs/jquery-ui'));

        // Copy highcharts
        // gulp.src(paths.bower + 'chart.js/dist/**/*.**')
        // .pipe(gulp.dest(paths.theme + 'js/libs/chart.js'));

        // Copy moment
        // gulp.src(paths.bower + 'moment/**/**/*.**')
        /// .pipe(gulp.dest(paths.theme + 'js/libs/moment'));

        // Copy datatables
        // gulp.src(paths.dev + 'datatables.net/js/*.**')
        // .pipe(gulp.dest(paths.theme + 'js/libs/datatables'));
        // gulp.src(paths.dev + 'datatables.net-bs4/js/*.**')
        // .pipe(gulp.dest(paths.theme + 'js/libs/datatables'));

        // Copy Lazyload
        gulp.src(paths.bower + 'vanilla-lazyload/dist/*.**')
        .pipe(gulp.dest(paths.theme + 'js/libs/lazyload'));
        
        // Copy JPlayer
        gulp.src(paths.bower + 'jPlayer/dist/jplayer/*.**')
        .pipe(gulp.dest(paths.theme + 'js/libs/jPlayer'));

        // Copy JPlayer playlist
        gulp.src(paths.bower + 'jPlayer/dist/add-on/*.**')
        .pipe(gulp.dest(paths.theme + 'js/libs/jPlayer/playlist'));

        // Copy Slick Carousel Fonts
        // gulp.src(paths.bower + 'slick-carousel/slick/fonts/**/*.{otf,ttf,woff,woff2,eot,svg}')
        // .pipe(gulp.dest(paths.theme + 'fonts'));

        //Copy Slick Slider Images
        // gulp.src(paths.bower + 'slick-carousel/slick/*.{jpg,jpeg,png,gif}')
        // .pipe(gulp.dest(paths.theme + 'images'));

        // Copy intl-tel-input Images
        // gulp.src(paths.bower + 'intl-tel-input/build/img/*.{jpg,jpeg,png,gif}')
        // .pipe(gulp.dest(paths.theme + 'images'));

        //gulp.src(paths.bower + 'bootstrap-formhelpers/dist/img/*.{jpg,jpeg,png,gif}')
        //.pipe(gulp.dest(paths.theme + 'images'));

        done();

    };

    exports.copyAssets = copyAssets;

    // Run:
    // gulp copyFonts
    function copyFonts() {
        return gulp.src(paths.dev + 'fonts/**/*.*')
        .pipe(gulp.dest(paths.theme + 'fonts'))
        .pipe(reload({stream: true}));
    };

    exports.copyFonts = copyFonts;

    // Run:
    // gulp images.
    // Copy and minify theme images
    function images() {
        return gulp.src(paths.dev + 'images/**/*.{jpg,jpeg,png,gif,ico,svg}')
        .pipe(newer(paths.theme + 'images'))
        //.pipe(rimraf({ force: true }))
        .pipe(gulpif(env === 'development', imagemin({
            svgoPlugins: [{ removeViewBox: false }],
        })))
        .pipe(gulpif(env === 'production', imagemin({
            optimizationLevel: 7,
            progressive: true,
            interlaced: true,
            svgoPlugins: [{ removeViewBox: false }],
            use: [pngcrush()]
        })))
        .pipe(gulp.dest(paths.theme + 'images'))
        .pipe(reload({stream:true}));
    };

    exports.images = images;


    // Run:
    // gulp allImages.
    // Copy and minify theme all images
    function allImages() {
        return gulp.src(paths.dev + 'images/**/*.{jpg,jpeg,png,gif,ico,svg}')
        .pipe(gulpif(env === 'development', imagemin({
            svgoPlugins: [{ removeViewBox: false }],
        })))
        .pipe(gulpif(env === 'production', imagemin({
            optimizationLevel: 7,
            progressive: true,
            interlaced: true,
            svgoPlugins: [{ removeViewBox: false }],
            use: [pngcrush()]
        })))
        .pipe(gulp.dest(paths.theme + 'images'))
        .pipe(reload({stream:true}));
    };

    exports.allImages = allImages;



    // Run:
    // gulp browserSync
    // Starts browser-sync task for starting the server.
    function browserSyncStart(done) {
        browserSync.init(browserSyncWatchFiles, browserSyncOptions);
        done();
    };

    exports.browserSync = browserSync;


    // Run:
    // gulp watchTask
    // Starts watcher. Watcher runs gulp sass task on changes
    function watchTask(done){
        gulp.watch(paths.dev + 'scss/**/*.scss', styles);
        //gulp.watch(paths.dev + 'scss/**/*.scss', stylesMin);
        gulp.watch(paths.dev + 'scripts/*.js', scripts);
        gulp.watch(paths.dev + 'scripts/custom/*.js', customScripts);
        gulp.watch(paths.dev + 'scripts/pages/*.js', pageScripts);
        gulp.watch(paths.dev + 'scripts/vendor/*.js', vendorScripts);
        gulp.watch(paths.dev + 'scripts/json/*.json', jsonScripts);
        gulp.watch(paths.dev + 'images/**/*.{jpg,jpeg,png,gif,ico,svg}', images);
        gulp.watch(paths.dev + 'fonts/**/*.*', copyFonts);

        done();
    }

    // Run:
    // gulp
    // Starts watcher with browser-sync. Browser-sync reloads page automatically on your browser
    exports.default = gulp.series(
        styles,
        stylesMin,
        scripts,
        customScripts,
        pageScripts,
        vendorScripts,
        images,
        jsonScripts,
        copyAssets,
        copyFonts,
        browserSyncStart,
        watchTask
    );


})();

