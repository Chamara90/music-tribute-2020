/**
 *
 * @mainSection: OTP varify page;
 * @dateCreated: 20190917;
 * @by : lakmal@fortunaglobal.com;
 *
 */

var preloaderHide = function(){
    $('.preloader').fadeOut(300);
};

var preloaderShow = function(){
    $('.preloader').fadeIn(300);
};



(function( $ ) {
    "use strict";


    var $otpInput = $('#otp_pin').pincodeInput({
        inputs: 5,
        complete:function(value, e, errorElement){
            console.log("code entered: " + value);
            // console.log("code entered: " + value);
            // $.ajax({
            //     url: '',
            //     type: 'POST',
            //     data: $otpForm.serialize(),
            //     dataType: 'JSON',
            //     beforeSend: function () {
         //            preloaderHide();
            //     },
            //     complete: function () {
         //            preloaderShow();
            //     },
            //     success: function (data) {
            //         console.log(data);
            //     }
            // });
            //  $(errorElement).text('Test');
        }
    });

    $otpInput.data('plugin_pincodeInput').focus();

}(jQuery));
