/**
 *
 * @mainSection: Change Password;
 * @dateCreated: 20191205;
 * @by : lakmal@fortunaglobal.com;
 *
 */


var preloaderHide = function(){
    $('.preloader').fadeOut(300);
};

var preloaderShow = function(){
    $('.preloader').fadeIn(300);
};


(function( $ ) {

    $('#change-password-form').submit(function(e){
        e.preventDefault();
    });

    $('#change-password-form').validate({
        ignore: [],
        rules: {
            current_password: {
                required: true,
                maxlength: 50,
            },
            password: {
                required: true,
                normalizer: function( value ) {
                    return $.trim( value );
                },
                minlength: 7,
                maxlength: 50,
            },
            confirm_password: {
                equalTo: "#password"
            },
        },
        messages: {
            current_password: {
                required: "Please enter your current password",
            },
            password:{
                required: "Please enter a new password",
                minlength: "A minimum of 8 characters are required",
                maxlength: "Only 50 characters are allowed",
            },
            confirm_password:{
                equalTo: "Password fields do not match!",
            }
        },
        submitHandler: function(form) {
            form.submit();
            // $.ajax({
            //     url: $('#root').val(),
            //     type: 'POST',
            //     dataType: 'JSON',
            //     headers: {
            //         'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            //     },
            //     data:{
            //         "old" : function(){
            //             return $('#current_password').val();
            //         },
            //         "new" : function(){
            //             return $('#password').val();
            //         }
            //     },
            //     beforeSend: function () {
            //         preloaderShow();
            //     },
            //     complete: function () {
            //         preloaderHide();
            //     },
            //     success: function(response){
            //         if(response.success){

            //         } else{
            //         }
            //     }
            // });
        }
    });


}(jQuery));




(function( $ ) {

    $('#reset-password-form').submit(function(e){
        e.preventDefault();
    });

    $('#reset-password-form').validate({
        ignore: [],
        rules: {
            username: {
                required: true,
                normalizer: function( value ) {
                    return $.trim( value );
                },
            },
        },
        submitHandler: function(form) {
            form.submit();
            // $.ajax({
            //     url: $('#root').val(),
            //     type: 'POST',
            //     dataType: 'JSON',
            //     headers: {
            //         'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            //     },
            //     data:{
            //         "old" : function(){
            //             return $('#current_password').val();
            //         },
            //         "new" : function(){
            //             return $('#password').val();
            //         }
            //     },
            //     beforeSend: function () {
            //         preloaderShow();
            //     },
            //     complete: function () {
            //         preloaderHide();
            //     },
            //     success: function(response){
            //         if(response.success){

            //         } else{
            //         }
            //     }
            // });
        }
    });


}(jQuery));


