/**
 *
 * @mainSection: Login;
 * @dateCreated: 20191205;
 * @by : lakmal@fortunaglobal.com;
 *
 */


var preloaderHide = function(){
    $('.preloader').fadeOut(300);
};

var preloaderShow = function(){
    $('.preloader').fadeIn(300);
};


(function( $ ) {

    $('#contact-form').submit(function(e){
        e.preventDefault();
    });

    $('#contact-form').validate({
        ignore: [],
        rules: {
            fname: {
                required: true,
                normalizer: function( value ) {
                    return $.trim( value );
                },
            },
            email: {
                required: true,
                normalizer: function( value ) {
                    return $.trim( value );
                },
                customEmail: true,
                maxlength: 100,
            },
            phone: {
                required: true,
                number: true,
                normalizer: function( value ) {
                    return $.trim( value );
                },
                minlength: 9,
                maxlength: 10,
            },
            message: {
                required: true,
                normalizer: function( value ) {
                    return $.trim( value );
                },
                maxlength: 500,
            }
        },
        submitHandler: function(form) {
            form.submit();
            // $.ajax({
            //     url: '',
            //     type: 'POST',
            //     dataType: 'JSON',
            //     headers: {
            //         'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            //     },
            //     data: $(form).serialize(),
            //     beforeSend: function () {
            //         preloaderShow();
            //     },
            //     complete: function () {
            //         preloaderHide();
            //     },
            //     success: function(response){
            //         if(response.success){
            //             $(form).find('.alert-success').removeClass('hide');
            //         } else{
            //             $(form).find('.alert-error').removeClass('hide');
            //         }
            //     }
            // });
        }
    });


}(jQuery));


