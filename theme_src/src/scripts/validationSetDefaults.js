jQuery.validator.setDefaults({
    errorPlacement: function (error, element) {
        if (element.parent().hasClass('input-group')) {
            if(element.hasClass('input-number')){
                element.closest('.form-group').append(error);
            }  else{
                element.closest('.input-group').after(error);
            }
        } else if(element.parent().hasClass('bootstrap-select')) {
            element.closest('.bootstrap-select').after(error);
        } else if(element.hasClass('input_phone')){
            element.closest('.iti').after(error);
        } else if(element.attr('type') === 'checkbox'){
            if(element.hasClass('custom-control-input')){
                element.closest('.custom-control').after(error);
            } else{
                element.closest('.form-check').after(error);
            }
        } else if(element.attr('type') === 'file') {
            if(element.hasClass('crop_image')){
                element.closest('.upload-image').after(error);
            } else if (element.hasClass('custom-file-input')){
                element.closest('.custom-file').after(error);
            } else{
                error.insertAfter(element);
            }
        } else {
            error.insertAfter(element);
        }
    },
    // onfocusout: function (element, event) {
    //     //Get Objects
    //     $element = $(element);
    //     //Remove whitespace
    //     if ( $element.is('input') && $element.attr('type') != 'file' || $element.is('textarea') ) {
    //         $element.val($.trim($element.val()));
    //     }
    // },
});
