/**
 *
 * @mainSection: Upload and Crop;
 * @dateCreated: 20190919;
 * @by : lakmal@fortunaglobal.com;
 *
 */


(function ($) {
    "use strict";

    var $uploadCrop;

    var readFile = function (input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();
            reader.onload = function (e) {
                $uploadCrop.croppie('bind', {
                    url: e.target.result
                });
            };
            reader.readAsDataURL(input.files[0]);
        } else {
            swal("Sorry - you're browser doesn't support the FileReader API");
        }
    };

    var resetCroppie = function () {
        destroyCroppie();
        initCroppie();
    };

    var destroyCroppie = function () {
        $uploadCrop.croppie('destroy');
    };

    var initCroppie = function () {
        if($(window).width() > 576){
            $uploadCrop = $('.image-crop').croppie({
                enableExif: true,
                viewport: {
                    width: 250,
                    height: 250,
                },
                boundary: {
                    width: 400,
                    height: 400
                }
            });
        } else{
            $uploadCrop = $('.image-crop').croppie({
                enableExif: true,
                viewport: {
                    width: 200,
                    height: 200,

                },
                boundary: {
                    width: 250,
                    height: 250
                }
            });
        }
    };

    initCroppie();

    $(document).on('click', '.upload-image-link', function(e) {
        e.preventDefault();
        $(this).closest('.upload-element').find('input[type="file"]').click();
    });

    $(document).on('change', '.upload-element input[type="file"]', function (event) {
        $('#upload-modal').attr('data-file-input', '');
        var $this = this;
        if ($(this).valid()) {
            $('#upload-modal').attr('data-file-input', $(this).attr('id'));
            $('#upload-modal').on('shown.bs.modal', function () {
                readFile($this);
            });
            $('#upload-modal').modal('show');
        }
    });

    $('#upload-modal .btn-save').on('click', function(e) {
        var fileInput = $('#upload-modal').attr('data-file-input');
        var parentUploadEle = $('#' + fileInput).closest('.upload-element');

        $uploadCrop.croppie('result', {
            type: 'base64',
            size: {width: 150, height: 150},
            format: 'png',
        }).then(function (resp) {
            parentUploadEle.find('.upload-image').find('input[type="hidden"]').val(resp);
            parentUploadEle.find('.upload-image').find('img').attr('src', resp);
            parentUploadEle.addClass('has-image');
            $('#upload-modal').modal('hide');
            resetCroppie();
        });
    });

    $('#upload-modal .btn-close').on('click', function(e) {
        var fileInput = $('#upload-modal').attr('data-file-input');
        var parentUploadEle = $('#' + fileInput).closest('.upload-element');
        $('#upload-modal').modal('hide');
        resetCroppie();
        parentUploadEle.find('.upload-image').find('input[type=file]').val('');
    });

    $(document).on('click', '.remove-image-link', function(e) {
        e.preventDefault();

        $this = $(this);

        $this.closest('.upload-element').find('.upload-image').find('img').attr('src', defaultImage);
        $this.closest('.upload-element').find('input[type="hidden"], input[type="file"]').val('');
        $this.closest('.upload-element').removeClass('has-image');
    });

}(jQuery));
