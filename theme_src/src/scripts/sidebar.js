/**
 *
 * @mainSection: Sidebar;
 * @dateCreated: 20191217;
 * @by : lakmal@fortunaglobal.com;
 *
 */


(function($) {
    "use strict";

    $(document).on('click', '.fs-toggle', function(){
        $(this).toggleClass('collapsed');
        $(this).next('.collapse').collapse('toggle');
    });

    $(document).on('click', '.btn-more', function(){
        $(this).toggleClass('collapsed');
        $(this).prev('.collapse').collapse('toggle');
    });

})(jQuery);
