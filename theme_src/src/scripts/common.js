$.fn.scrollEnd = function(callback, timeout) {
    $(this).scroll(function(){
        var $this = $(this);
        if ($this.data('scrollTimeout')) {
            clearTimeout($this.data('scrollTimeout'));
        }
        $this.data('scrollTimeout', setTimeout(callback,timeout));
    });
};

var headerSticky = function(){
    var scroll = $(window).scrollTop();
    if (scroll > $('#header').height()) {
        $('#header').addClass("sticky");
    } else {
        $('#header').removeClass("sticky");
    }
};


$(document).ready(function() {

    $('[data-toggle="tooltip"]').tooltip();

    // AOS.init();


});

(function($) {
    "use strict";

    headerSticky();

    $(window).on('scroll', function(){
        headerSticky();
    });

    $(window).scrollEnd(function(){
        $('#header').removeClass("sticky");
    }, 300);

    $(document).on('click', '.navbar-toggler', function(e){
        e.stopPropagation();
        $(this).toggleClass('oppenned');
        $('.mobile-sidebar, .mobile-sidebar-bg').toggleClass('oppenned');
    });

    $(document).on('click', '.mobile-sidebar-close', function(e){
        e.stopPropagation();
        $('.mobile-sidebar, .mobile-sidebar-bg').removeClass('oppenned');
        $('.navbar-toggler').removeClass('oppenned');
    });

    $(document).on('click', function (e) {
        var container = $(".mobile-sidebar");
        if (!container.is(e.target) && container.has(e.target).length === 0){
            if($('.mobile-sidebar').hasClass('oppenned')){
                $('.mobile-sidebar, .mobile-sidebar-bg').removeClass('oppenned');
                $('.navbar-toggler').removeClass('oppenned');
            }
        }
    });

    // $(document).on('click', '.side-menu .dropdown > a', function() {
    //     $(this).closest('.nav').find('.collapse').collapse('hide');
    //     $(this).closest('.nav').find('.nav-item.dropdown').removeClass('open');
    //     $(this).next('div').collapse('show');
    //     $(this).parent('.dropdown').addClass('open');
    //     return false;
    // });

}(jQuery));

