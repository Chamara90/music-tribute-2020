/*
 *
 *
 * intlTelInput validations
 *
 *
 */

var errorMap = ["Invalid number", "Invalid country code", "Too short", "Too long", "Invalid number"];

var itiValidation = function(itiInput, iti){
    var labelError;
    itiInput.addEventListener('keyup', function() {
        if(itiInput.value.length > 1){
            if (itiInput.value.trim()) {
                if (iti.isValidNumber()) {
                    itiInput.classList.remove("invalid-phone");
                    itiInput.setAttribute('data-error-msg', '');
                } else {
                    var errorCode = iti.getValidationError();
                    itiInput.classList.add("invalid-phone");
                    itiInput.setAttribute('data-error-msg', errorMap[errorCode]);
                }
            }
        } else{
            itiInput.classList.remove("invalid-phone");
            itiInput.setAttribute('data-error-msg', '');
        }
    });
};


(function($) {
    "use strict";

    $(document).on('change', '.custom-file-input', function(){
        $(this).next('.custom-file-label').text($(this).val().replace(/^.*\\/, ""));
    });

    $(window).on('load', function(){
        $('.custom-file-input').each(function(){
            if($(this).val()){
                $(this).next('.custom-file-label').text($(this).val().replace(/^.*\\/, ""));
            }
        });
    });

    $(document).on('click', '.custom-switch-toggle label', function(){
        $('.custom-switch-toggle label').removeClass('on');
        $(this).addClass('on');
    });

    $(document).on('click', '.custom-switch-toggle label.on', function(e){
        e.preventDefault();
    });


    setTimeout(function() {
        $('.has-label-animate .form-control').each(function(){
            var textVal = $(this).val();
            if (textVal === "") {
                $(this).closest('.has-label-animate').removeClass('has-value');
            } else {
                $(this).closest('.has-label-animate').addClass('has-value');
            }
        });
    });

    $(document).on('focus', '.has-label-animate', function(){
        $(this).addClass('has-value');
    });

    $(document).on('focusout blur', '.has-label-animate input, .has-label-animate textarea', function(){
        var textVal = $(this).val();
        if (textVal === "") {
            $(this).closest('.has-label-animate').removeClass('has-value');
        } else {
            $(this).closest('.has-label-animate').addClass('has-value');
        }
    });

    // $(document).on('focus', '.has-label-animate select', function () {
    //     $(this).closest('.has-label-animate').addClass('has-value');
    // });

    $(document).on('change', '.has-label-animate select', function () {
        var textVal = $(this).val();
        if (textVal === "") {
            $(this).closest('.has-label-animate').removeClass('has-value');
        } else {
            $(this).closest('.has-label-animate').addClass('has-value');
        }
    });
}(jQuery));


