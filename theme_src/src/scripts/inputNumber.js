/**
 *
 * @mainSection: Input number;
 * @dateCreated: 20191009;
 * @by : lakmal@fortunaglobal.com;
 *
 */

(function( $ ) {
    "use strict";

    $('.btn-number').click(function(e){
        e.preventDefault();

        fieldName = $(this).attr('data-field');
        type = $(this).attr('data-type');
        var input = $("input[name='"+fieldName+"']");
        var currentVal = parseInt(input.val());
        if (!isNaN(currentVal)) {

            if(type == 'minus') {

                if(input.attr('min')){
                    if(currentVal > input.attr('min')) {
                        input.val(currentVal - 1).change();
                    }
                } else{
                    input.val(currentVal - 1).change();
                }
                // if(parseInt(input.val()) == input.attr('min')) {
                //     $(this).attr('disabled', true);
                // }

            } else if(type == 'plus') {

                if(input.attr('max')){
                    if(currentVal < input.attr('max')) {
                        input.val(currentVal + 1).change();
                    }
                } else{
                    input.val(currentVal + 1).change();
                }
                // if(parseInt(input.val()) == input.attr('max')) {
                //     $(this).attr('disabled', true);
                // }

            }
        } else {
            if(type == 'minus') {

                input.val('');

            } else if(type == 'plus') {

                input.val(1);

            }
        }
        input.valid();
    });

}(jQuery));
